<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::view('/', 'welcome');

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout');
    Route::post('refresh', 'Auth\LoginController@refresh');
    Route::get('me', 'Auth\LoginController@me');
});

Route::middleware('auth:api')->group(function () {

    Route::prefix('autocomplete')->group(function () {
        Route::get('news', 'NewsController@autocomplete');
        Route::get('resolutions', 'ResolutionController@autocomplete');
    });

    Route::prefix('upload')->group(function () {
        Route::get('{upload}', 'UploadController@show')->where('upload', '[0-9]+');
        Route::post('{type}/{public}/{folder?}', 'UploadController@upload');
        Route::get('generate/{hash}', 'UploadController@generate');
    });

    Route::prefix('news')->group(function () {
        Route::post('', 'NewsController@store');
        Route::prefix('disposition')->group(function () {
            Route::get('', 'NewsController@disposition');
            Route::post('', 'NewsController@saveDisposition');
        });
    });

    Route::get('users/roles', 'UserController@getRoles');

    Route::prefix('users')->group(function () {
        Route::post('edit/{id}', 'UserController@update');
        Route::post('delete/{id}', 'UserController@destroy');
    });

    Route::apiResource('users', 'UserController');

    Route::prefix('news')->group(function () {
        Route::post('edit/{id}', 'NewsController@update');
        Route::post('delete/{id}', 'NewsController@destroy');
    });

    Route::apiResource('news', 'NewsController');

    Route::prefix('pages')->group(function () {
        Route::post('edit/{id}', 'PageController@update');
        Route::post('delete/{id}', 'PageController@destroy');
    });

    Route::apiResource('pages', 'PageController');

    Route::prefix('resolutions')->group(function () {
        Route::get('tree', 'ResolutionController@treeIndex');
        Route::post('edit/{id}', 'ResolutionController@update');
        Route::post('delete/{id}', 'ResolutionController@destroy');
    });

    Route::apiResource('resolutions', 'ResolutionController');

    Route::prefix('banners')->group(function () {
        Route::post('edit/{id}', 'BannerController@update');
        Route::post('delete/{id}', 'BannerController@destroy');
    });

    Route::apiResource('banners', 'BannerController');

    Route::apiResource('profile', 'ProfileController');

    Route::prefix('profile')->group(function () {
        Route::post('edit/{id}', 'ProfileController@update');
    });
});
