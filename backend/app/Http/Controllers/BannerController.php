<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\News;
use Illuminate\Http\Request;
use Validator;

class BannerController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function webIndex()
	{
		$banners = Banner::orderBy('sort', 'asc')->get();
		return $banners->map(function ($item, $key) {
			return [
				'image' => $item->image,
				'link' => $item->external_link,
				'slug' => $item->news_id ? News::find($item->news_id)->slug : null
			];
		});
	}

	public function index(Request $request) {
		$banners = Banner::all();
		return $banners->map(function($item) {
			if (!$item->image && $item->news_id) {
				$item->image = News::find($item->news_id)->image;
			}
			if (substr($item->image, 0, 1) === '/') {
				$item->image = asset($item->image);
			}
			return $item;
		});
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Models\Banner  $banner
	 * @return \Illuminate\Http\Response
	 */
	public function show(Banner $banner)
	{
		if (!$banner->image && $banner->news_id) {
			$banner->image = News::find($banner->news_id)->image;
		}
		if (substr($banner->image, 0, 1) === '/') {
			$banner->image = asset($banner->image);
		}
		if ($banner->news_id) {
			$article = News::find($banner->news_id);
			$banner->article = $article->title;
			$banner->article_id = $article->id;
		}
		return $banner;
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'image' => 'nullable|string',
			'article_id' => 'nullable|numeric',
			'external_link' => 'nullable|string'
		]);

		if ($validator->fails()) {
			return [
				'success' => false,
				'message' => $validator->errors()->first()
			];
		}

		$sort = Banner::count() + 1;
		$banner = Banner::create([
			'image_id' => $request->image_id,
			'news_id' => $request->article_id,
			'external_link' => $request->external_link,
			'sort' => $sort,
			'user_id' => $request->user()->id
		]);



		return ['success' => true, 'id' => $banner->id, 'message' => 'El banner se ha creado sin problemas'];
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Models\Banner  $banner
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Banner $banner, $id)
	{

		$validator = Validator::make($request->all(), [
			'image' => 'nullable|string',
			'article_id' => 'nullable|numeric',
			'external_link' => 'nullable|string'
		]);

		if ($validator->fails()) {
			return [
				'success' => false,
				'message' => $validator->errors()->first()
			];
		}
		$banner = Banner::findOrFail($id);
		$banner->image_id = $request->image_id;
		if ($request->article_id) {
			$banner->news_id = $request->article_id;
			$banner->external_link = null;
		} else {
			$banner->external_link = $request->external_link;
			$banner->news_id = null;
		}

		$banner->save();



		return ['success' => true, 'message' => 'El banner se ha actualizado'];
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Models\Banner  $banner
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Banner $banner, $id)
	{
		Banner::findOrFail($id)->delete();
		return ['success' => true];
	}
}
