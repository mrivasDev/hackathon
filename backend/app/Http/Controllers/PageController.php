<?php

namespace App\Http\Controllers;

use App\Models\Page;
use App\Models\PageCard;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Validation\Rule;

class PageController extends Controller
{
	/**
	 * Display the specified resource by slug.
	 *
	 * @param  \App\Models\Page  $page
	 * @return \Illuminate\Http\Response
	 */
	public function showBySlug(Request $request, $slug)
	{
		if (! $page = Page::where('slug', $slug)->first()) {
			return response()->json(['message' => 'Page not found'], 404);
		}
		$page->load('cards');
		return $page;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Models\Page  $page
	 * @return \Illuminate\Http\Response
	 */
	public function show(Page $page)
	{
		$page->load('cards');
		return $page;
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Models\Page  $page
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Page $page, $id)
	{
		$validator = Validator::make($request->all(), [
			'body' => 'nullable|string',
			'type' => ['required', Rule::in(['basic', 'grid'])],
			'image_id' => 'nullable|exists:uploads,id',
			'cards.*.id' => 'nullable|exists:page_cards,id',
			'cards.*.image_id' => 'nullable|exists:uploads,id',
			'cards.*.title' => 'required|string',
			'cards.*.body' => 'nullable|string'
		]);

		if ($validator->fails()) {
			return ['success' => false, 'message' => $validator->errors()->first()];
		}

		$page = Page::findOrFail($id);

		$page->type = $request->type;
		$page->image_id = $request->image_id;
		$page->body = $request->body;
		$page->save();

		if ($page->type === 'grid' && is_array($request->cards)) {
			foreach ($request->cards as $card) {
				$card['page_id'] = $page->id;
				if (isset($card['id'])) {
					PageCard::findOrFail($card['id'])
						->update($card);
				} else {
					PageCard::create($card);
				}
			}
			if (count($request->cards) !== $page->cards()->count()) {
				foreach($page->cards as $existingCard) {
					$found = false;
					foreach ($request->cards as $card) {
						if (isset($card['id']) && intval($card['id']) === $existingCard['id']) {
							$found = true;
						}
					}
					if (!$found) {
						$existingCard->delete();
					}
				}
			}
		}

		return ['success' => true];
	}
}
