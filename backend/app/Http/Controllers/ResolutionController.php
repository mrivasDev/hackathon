<?php

namespace App\Http\Controllers;

use App\Models\Resolution;
use Illuminate\Http\Request;
use Validator;

class ResolutionController extends Controller
{

	public function index(Request $request)
	{
		$resolutions = [];
		$orderField = $request->orderField ? $request->orderField : 'id';
		$orderDirection = $request->orderDirection ? $request->orderDirection : 'desc';
		$resolutions = Resolution::orderBy($orderField, $orderDirection)->paginate($request->size, ['*'], 'page', $request->page + 1);
		return $resolutions;
	}

	public function treeIndex(Request $request)
	{
		$resolutions = [];
		$resolutions = Resolution::orderBy('year', 'desc')->get();
		return $resolutions;
	}


	public function store(Request $request)
	{
		$validator = Validator::make(
			$request->all(),
			[
				'year' => 'nullable|numeric',
				'subject' => 'nullable|string',
				'description' => 'required|string',
				'upload_id' => 'required|numeric'
				]
			);

			if ($validator->fails()) {
				return [
					'success' => false,
					'message' => $validator->errors()->first()
				];
			}

			$resolution = Resolution::create(
				[
					'year' => $request->year,
					'subject' => $request->subject,
					'description' => $request->description,
					'upload_id' => $request->upload_id
					]
				);

				return ['success' => true, 'id' => $resolution->id, 'message' => 'La resolución se ha creado sin problemas'];
			}


			public function show(Resolution $resolution)
			{
				return $resolution->load('upload');
			}


			public function update(Request $request, Resolution $resolution, $id)
			{
				$validator = Validator::make(
					$request->all(),
					[
						'year' => 'nullable|numeric',
						'subject' => 'nullable|string',
						'description' => 'required|string',
						'upload_id' => 'required|numeric'
						]
					);

					if ($validator->fails()) {
						return [
							'success' => false,
							'message' => $validator->errors()->first()
						];
					}

					$resolution = Resolution::findOrFail($id);

					$resolution->description = $request->description;
					$resolution->upload_id = $request->upload_id;
					$resolution->subject = $request->subject;
					$resolution->year = $request->year;
					$resolution->save();

					return ['success' => true, 'id' => $resolution->id, 'message' => 'La resolución se ha actualizado sin problemas'];
				}

				public function destroy(Resolution $resolution, $id)
				{
					Resolution::findOrFail($id)->delete();
					return ['success' => true, 'message' => 'La resolución se ha eliminado sin problemas'];
				}

				public function autocomplete(Request $request)
				{
					if (is_numeric($request->text)) {
						$query = Resolution::where('id', $request->text);
					} else {
						$query = Resolution::where('description', 'LIKE', '%' . $request->text . '%')
						->orderByRaw(
							"
							CASE
							WHEN `description` = '$request->text' THEN 0
							WHEN `description` LIKE '$request->text%' THEN 1
							WHEN `description` LIKE '%$request->text%' THEN 2
							WHEN `description` LIKE '%$request->text' THEN 3
							ELSE 4
							END"
						);
					}
					return $query->selectRaw("id, description, subject, year, upload_id, CONCAT(COALESCE(subject, 'Sin descripcion'),'(',description, ')') AS fullName")->get();
				}
			}
