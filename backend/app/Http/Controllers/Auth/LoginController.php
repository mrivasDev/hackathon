<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use GuzzleHttp\Client;
use Hash;
use Illuminate\Http\Request;
use Log;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt', ['except' => ['login']]);
    }
    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $user = null;
        if (filter_var($request->username, FILTER_VALIDATE_EMAIL)) {
            $user = User::where('email', strtolower($request->username))->first();
            if ($user) {
                $credentials = [
                    'dni' => $user->dni,
                    'password' => $request->password
                ];
            } else {
                return response()->json(['success' => false, 'message' => 'Credenciales inválidas'], 400);
            }
        } else {
            $credentials = [
                'dni' => $request->username,
                'password' => $request->password
            ];
        }

        if (!$user) {
            $user = User::where('dni', $credentials['dni'])->first();
        }

        try {
            $token = \JWTAuth::attempt($credentials);
        } catch (\JWTException $e) {
            return response()->json(['success' => false, 'message' => 'Error del sistema'], 500);
        }

        if ($token) {
            return $this->respondWithToken($token);
        } else {
            return response()->json(['success' => false, 'message' => 'Credenciales inválidas'], 400);
        }
    }
    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $user = auth()->user();
        $roles = $user->getRoleNames();
        $permissions = $user->getAllPermissions()->map(function ($p) {
            return $p->name;
        })->toArray();
        unset($user['roles']);
        unset($user['permissions']);
        $user['roles'] = $roles;
        $user['permissions'] = $permissions;
        return response()->json($user);
    }

    public function payload()
    {
        return response()->json(auth()->payload());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();
        return response()->json(['success' => true]);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        $user = auth()->user();
        $roles = $user->getRoleNames();
        $permissions = $user->getAllPermissions()->map(function ($p) {
            return $p->name;
        })->toArray();
        unset($user['roles']);
        $user['roles'] = $roles;
        $user['permissions'] = $permissions;
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => $user,
        ]);
    }
}
