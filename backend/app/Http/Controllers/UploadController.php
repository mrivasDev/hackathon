<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Upload;
use App\Models\UploadAccess;
use Carbon\Carbon;

class UploadController extends Controller
{
    public function upload(Request $request, $type, $public, $folder = '') {

		if (!$type) {
			return response()->json(['error' => 'File extension missing'], 400);
		}

		if (!$request->file('file')->isValid()) {
			return response()->json(['error' => 'File too big.'], 400);
		}

		$public = $public === 'public';

		$uploadedFile = $request->file('file');
		$extension = $uploadedFile->extension();

		if ($public) {
			$base_path = public_path() . '/media/' . $type . '/';
		} else {
			$base_path = storage_path() . '/app/' . $type . '/';
		}

		if (!file_exists($base_path)) {
			mkdir($base_path, 0775);
		}

		do {
			$basename = bin2hex(random_bytes(12));
			$filename = sprintf('%s.%0.8s', $basename, $extension);
			do {
				$subDirectory = bin2hex(random_bytes(1));
			} while ($subDirectory === 'ad');
			if (strlen($folder) > 0) {
				$path = $folder . '/' . $subDirectory . '/' . $filename;
			} else {
				$path = $subDirectory . '/' . $filename;
			}
		} while (file_exists($base_path . $path));

		if (strlen($folder) > 0 && !file_exists($base_path . $folder)) {
			mkdir($base_path . $folder, 0775);
		}

		if (!file_exists($base_path . $subDirectory)) {
			if (strlen($folder) > 0 && !file_exists($base_path . $folder . '/' . $subDirectory)) {
					mkdir($base_path . $folder . '/' . $subDirectory, 0775);
			} else {
				mkdir($base_path . $subDirectory, 0775);
			}
		}

		if ($type === 'image') {
			$img = \Image::make($uploadedFile->getRealPath());
			$img->resize(null, 720, function ($constraint) {
				$constraint->aspectRatio();
				$constraint->upsize();
			});
			$img->save($base_path . $path);
		} else if ($type === 'document') {
			if (!$uploadedFile->move(storage_path() . '/app/document/' . $folder . '/' . $subDirectory, $filename)) {
				return response()->json(['message' => 'Error moving the document'], 500);
			}
		} else {
			return response()->json(['message' => 'The file type is not valid'], 400);
		}

		$upload = Upload::create([
			'type' => $type,
			'name' => $uploadedFile->getClientOriginalName(),
			'folder' => strlen($folder) > 0 ? $folder : null,
			'file' => $subDirectory . '/' . $filename,
			'public' => $public
		]);

		if (!$upload->public) {
			$uploadAccess = UploadAccess::create([
				'upload_id' => $upload->id,
				'user_id' => auth()->user()->id
			]);
			$preview = url('upload/' . $uploadAccess->token);
		} else {
			$preview = asset('/media/' . $type . '/' . $path);
		}

		return [
			'name' => $upload->name,
			'id' => $upload->id,
			'preview' => $preview
		];
	}

	public function generate(Request $request, $hash)
	{
		if (! $upload = Upload::where('hash', $hash)->first()) {
			return response()->json(['message' => 'There is no such file'], 404);
		}

		if (! $uploadAccess = UploadAccess::where([
			['upload_id', $upload->id],
			['user_id', auth()->user()->id]
		])->first()) {
			$uploadAccess = UploadAccess::create([
				'upload_id' => $upload->id,
				'user_id' => auth()->user()->id
			]);
		}
		return $uploadAccess;
	}

	public function view(Request $request, $token)
	{
		if (! $uploadAccess = UploadAccess::where('token', $token)->first()) {
			abort(404);
		}
		if ($uploadAccess->expired_at->isBefore(Carbon::now())) {
			$uploadAccess->delete();
			abort(403);
		}
		$upload = Upload::findOrFail($uploadAccess->upload_id);
		$path = $upload->public ? public_path() : storage_path() . '/' . 'app';
		$path .= '/' . $upload->type;
		$path .= $upload->folder ? ('/' . $upload->folder) : '';
		$path .=  '/' . $upload->file;
		$file = file_get_contents($path);
		$headers = [
			'Content-type' => mime_content_type($path) . '; charset=utf-8',
			'Access-Control-Allow-Origin' => '*'
		];
		return response($file)->withHeaders($headers);
	}

	public function show(Request $request, Upload $upload)
	{
		if (!$upload->public) {
			$uploadAccess = UploadAccess::create([
				'upload_id' => $upload->id,
				'user_id' => auth()->user()->id
			]);
			$preview = url('upload/' . $uploadAccess->token);
		} else {
			$preview = asset('/media/' . $upload->type . '/' . ($upload->folder ? $upload->folder . '/' : '') . $upload->file);
		}
		$upload->preview = $preview;
		return $upload;
	}
}
