<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\News;
use App\Models\Variable;
use App\Models\Upload;

class NewsController extends Controller
{

	/**
	* Display a listing of the resource.
	*
	* @return \Illuminate\Http\Response
	*/
	public function index(Request $request)
	{
		$news = News::orderBy('id', 'desc')->paginate($request->cuantos, ['*'], 'pagina', $request->pag + 1);
		$news->getCollection()->transform(function ($article) {
			$image = $this->getImage($article);
			$article['url'] = $image;
			return $article;
		});

		return $news;
	}

	public function webIndex(Request $request)
	{
		$disposition = Variable::get('news_disposition')->json;
		if (count($disposition) > 0) {
			$articles_id = [];
			$news = collect();
			foreach ($disposition as $disp) {
				if ($disp['type'] === 'article') {
					array_push($articles_id, $disp['article']['id']);
				}
			}
			if (count($articles_id) < 5) {
				$latestNews = News::where('listed', true)->whereNotIn('id', $articles_id)->orderBy('id', 'desc')->take(5 - count($articles_id))->get();
			}
			foreach ($disposition as $disp) {
				if ($disp['type'] === 'article') {
					$news->push(News::find($disp['article']['id']));
				} else {
					$news->push($latestNews->shift());
				}
			}
		} else {
			$news = News::where('listed', true)->orderBy('id', 'desc')->take(5)->get();
		}
		$news = $news->map(function ($article) { return $this->cleanArticle($article); })->toArray();
		return $news;
	}

	private function cleanArticle($article, $deleteBody = true) {
		$article->date = Carbon::parse($article->created_at)->formatLocalized('%d de %B');
		$article->image = $this->getImage($article);
		unset($article->user_id);
		unset($article->created_at);
		unset($article->updated_at);
		unset($article->id);
		if ($deleteBody) {
			unset($article->body);
		}

		return $article;
	}

	/**
	* Store a newly created resource in storage.
	*
	* @param  \Illuminate\Http\Request  $request
	* @return \Illuminate\Http\Response
	*/
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'title' => 'required|string|min:8',
			'body' => 'required|string',
			]);

			if ($validator->fails()) {
				return [
					'success' => false,
					'message' => $validator->errors()->first()
				];
			}

			if (! $slug = $this->slugify($request->title)) {
				return [
					'success' => false,
					'message' => 'Error'
				];
			}

			if (!$request->image_id) {
				$request->image_id = 1;
			}

			if (!$request->display_date) {
				$request->display_date = 1;
			}

			$news = News::create([
				'title' => $request->title,
				'body' => $request->body,
				'image_id' => $request->image_id,
				'display_date' => false,
				'slug' => $slug,
				'user_id' => $request->user()->id,
				'listed' => true
				]);
				return ['success' => true, 'id' => $news->id, 'message' => 'The article was created with no problems'];
			}

			/**
			* Display the specified resource.
			*
			* @param  \App\Models\News  $news
			* @return \Illuminate\Http\Response
			*/
			public function show(News $news)
			{
				return $news;
			}

			/**
			* Display the specified resource by slug.
			*
			* @param  \Illuminate\Http\Request  $request
			* @param  string  $slug
			* @return \Illuminate\Http\Response
			*/
			public function showBySlug(Request $request, $slug) {
				if (! $article = News::where('slug', $slug)->first()) {
					return response()->json(['message' => 'not-found'], 404);
				}
				$article = $this->cleanArticle($article, false);
				// Get description from parsed body
				$article->description = strip_tags(html_entity_decode($article->body));
				$article->description = trim(urldecode(str_replace('%C2%A0', '', urlencode($article->description))));

				if (strlen($article->description) === 0) {
					$article->description = null;
				} else {
					// Concat dots for long body
					$dots = strlen($article->description) > 200;
					$article->description = substr($article->description, 0, 200);
					if ($dots) {
						if (substr($article->description, 199) === ' ') {
							// Cut out final space
							$article->description = substr($article->description, 0, 199);
						}
						$article->description .= '...';
					}
				}
				$article->description = str_replace('?', '', mb_convert_encoding($article->description, 'UTF-8', 'UTF-8'));
				return $article;
			}

			/**
			* Update the specified resource in storage.
			*
			* @param  \Illuminate\Http\Request  $request
			* @param  \App\Models\News  $news
			* @return \Illuminate\Http\Response
			*/
			public function update(Request $request, News $news, $id)
			{
				$validator = Validator::make($request->all(), [
					'title' => 'required|string|min:8',
					'body' => 'required|string'
					]);

					if ($validator->fails()) {
						return [
							'success' => false,
							'message' => $validator->errors()->first()
						];
					}

					if (! $slug = $this->slugify($request->title)) {
						return [
							'success' => false,
							'message' => 'Error'
						];
					}

					$news = News::findOrFail($id);

					$news->title = $request->title;
					$news->body = $request->body;
					$news->image_id = $request->image_id;
					$news->display_date = false;
					$news->slug = $slug;
					$news->listed = true;

					$news->save();

					return ['success' => true, 'message' => 'The article was updated with no problems'];
				}

				/**
				* Remove the specified resource from storage.
				*
				* @param  \App\Models\News  $news
				* @return \Illuminate\Http\Response
				*/
				public function destroy(News $news, $id)
				{
					News::findOrFail($id)->delete();
					return ['success' => true, 'message' => 'The article was removed with no problems'];
				}

				public function disposition(Request $request)
				{
					return Variable::get('news_disposition')->json;
				}

				public function saveDisposition(Request $request)
				{
					Variable::set('news_disposition', $request->disposition);
					return ['success' => true];
				}

				public function autocomplete(Request $request)
				{
					if (is_numeric($request->text)) {
						$query = News::where('listed', true)->where('id', $request->text);
					} else {
						$query = News::where('listed', true)->where('title', 'LIKE', '%' . $request->text . '%');
					}
					return $query->select('id', 'title')->take(10)->get();
				}

				private function getImage($article) {
					$upload = Upload::find($article->image_id);
					if ($upload) {
						return url('media/image/news/' . $upload->file);
					} else {
						return url('media/image/news/' . Upload::first()->file);
					}
				}

				function slugify($text) {
					$originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
					$modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
					$text = utf8_decode($text);
					$text = strtr($text, utf8_decode($originales), $modificadas);
					$text = utf8_encode($text);
					$text = strtolower($text);
					$text = preg_replace('~[^\pL\d]+~u', '-', $text);
					$text = preg_replace('~[^\pL\d]+~u', '-', $text);
					$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
					$text = preg_replace('~[^-\w]+~', '', $text);
					$text = trim($text, '-');
					$text = preg_replace('~-+~', '-', $text);
					$text = strtolower($text);
					if (empty($text)) {
						return null;
					}
					return $text;
				}

			}
