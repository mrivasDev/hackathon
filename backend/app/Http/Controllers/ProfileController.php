<?php

namespace App\Http\Controllers;
use App\Models\Profile;
use Validator;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Upload;

class ProfileController extends Controller
{

	public function index()
	{
		$user = auth()->user();
		$image = $this->getImage($user);
		$user['url'] = $image;
		$roles = $user->getRoleNames();
		$permissions = $user->getAllPermissions()->map(function ($p) {
			return $p->name;
		})->toArray();
		unset($user['roles']);
		unset($user['permissions']);
		$user['roles'] = $roles;
		$user['permissions'] = $permissions;
		return response()->json($user);
	}

	public function update(Request $request)
	{
		$authUser = auth()->user();

		$request->dni = strval(intval($request->dni));
		$validator = Validator::make($request->all(), [ 'name' => 'required|string|min:1|max:50', 'surname' => 'string|min:1|max:50', 'email' => 'required|email']);

		if ($validator->fails()) {
			return ['success' => false, 'message' => $validator->errors()->first()];
		}
		$user = User::findOrFail($authUser->id);
		$user->name = $request->name;
		$user->surname = $request->surname;
		$user->email = $request->email;
		$user->dni = $request->dni;
		$user->image_id = $request->image_id;
		$user->save();
		return ['success' => true, 'message' => 'Los datos se han actualizado sin problemas.'];
	}

	private function getImage($user) {
		$upload = Upload::find($user->image_id);
		if ($upload) {
			return url('media/image/usuarios/' . $upload->file);
		} else {
			return null;
		}
	}
}
