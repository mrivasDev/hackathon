<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class UploadAccess extends Model
{
    public $timestamps = false;

	protected $fillable = ['token', 'user_id', 'upload_id', 'expire_at'];

	protected $casts = [
        'expired_at' => 'datetime',
    ];
    
	public static function boot()
	{
		parent::boot();
		static::creating(function ($model) {
			do {
				$model->token = bin2hex(random_bytes(24));
			} while (UploadAccess::where('token', $model->token)->exists());
			$model->expired_at = Carbon::now()->addHour();
		});
	}

	public function upload()
    {
        return $this->belongsTo(Upload::class);
    }
}
