<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banner extends Model
{
	use SoftDeletes;
	protected $fillable = [
		'sort', 'image_id', 'external_link', 'news_id', 'user_id'
	];

	/**
	 * The accessors to append to the model's array form.
	 *
	 * @var array
	 */
	protected $appends = ['image'];

	public function getImageAttribute() {
		if ($this->image_id) {
			$upload = Upload::find($this->image_id);
			if ($upload) {
				return url('media/image/banners/' . $upload->file);
			}
		} else if ($this->news_id) {
			$article = News::find($this->news_id);
			if ($article) {
				return url('media/image/noticias/' . Upload::find($article->image_id)->file);
			}
		}
		return url('media/image/noticias/' . Upload::first()->file);
	}
}
