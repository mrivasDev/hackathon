<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = [
        'title', 'slug', 'body', 'type', 'image_id'
    ];

    protected $appends = ['url'];

    public function getUrlAttribute()
    {
    	if (!$this->image) {
            unset($this->image);
    		return null;
    	}
    	$file = $this->image->file;
        unset($this->image);
        return url('media/image/paginas/' . $file);
    }

    public function image()
    {
    	return $this->belongsTo(Upload::class, 'image_id', 'id');
    }

    public function cards()
    {
    	return $this->hasMany(PageCard::class);
    }
}
