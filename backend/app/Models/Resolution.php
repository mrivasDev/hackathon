<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Resolution extends Model
{
    protected $fillable = ['description', 'upload_id', 'year', 'subject'];

    public function upload()
    {
        return $this->belongsTo(Upload::class);
    }
}
