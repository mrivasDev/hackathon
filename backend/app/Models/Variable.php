<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Variable extends Model
{
    public $timestamps = false;
	protected $fillable = [
		'name', 'json', 'string', 'integer', 'boolean', 'timestamp'
	];

	protected $casts = ['json' => 'array'];

	public static function get($name)
	{
		if (! $variable = Variable::where('name', $name)->first()) {
			$datos = [
				'name' => $name
			];
			switch ($name) {
				case 'news_disposition':
					$datos['json'] = [];
				default:
					break;
			}
			$variable = Variable::create($datos);
		}
		return $variable;
	}

	public static function set($name, $valor = NULL)
	{
		$variable = Variable::where('name', $name)->first();
		if ($valor) {
			switch ($name) {
				case 'news_disposition':
					$variable->json = $valor;
					break;
				default:
					break;
			}
		} else {
			switch ($name) {
				default:
					break;
			}
		}
		$variable->save();
	}
}
