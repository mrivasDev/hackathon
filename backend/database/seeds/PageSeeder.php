<?php

use Illuminate\Database\Seeder;
use App\Models\Page;

class PageSeeder extends Seeder
{
    /**
    * Run the database seeds.
    * php artisan migrate:refresh --seed
    *  Agregar cada body que sea el HTML!!!
    * @return void
    */
    public function run()
    {
        Page::create(['title'=>'Marketplace security baseline', 'slug'=>'mp-security-baseline' ]);
        Page::create(['title'=>'MFA for Marketplace', 'slug'=>'mfa-for-mp' ]);
        Page::create(['title'=>'GDPR (General Data Protection Regulation)', 'slug'=>'gdpr' ]);
        Page::create(['title'=>'Certifications', 'slug'=>'certifications' ]);
        Page::create(['title'=>'Security Advisors', 'slug'=>'security-advisors' ]);
    }
}
