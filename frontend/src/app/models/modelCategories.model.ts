export class ModelCategories {
	constructor(
		public label: string,
		public id?: number,
		public category_parent?: number,
		public parent?: string,
		public upload_id?: number,
		) { }
	}
