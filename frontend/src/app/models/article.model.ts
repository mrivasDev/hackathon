export class Article {
    constructor(
        public id?: number,
        public title?: string,
        public body?: string,
        public image?: string,
        public display_date?:boolean,
        public description?: string,
        public slug?: string,
        public user_id?: number,
    ) { }
}