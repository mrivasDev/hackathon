export class User {
	constructor(
		public id?: number,
		public name?: string,
		public surname?: string,
		public dni?: string,
		public email?: string,
		public roles?: string[],
		public role?: string,
		public permissions?: string[],
		public image_id?: number,
		public url?: string,
	) {}
}
