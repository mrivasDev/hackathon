export class News {
  constructor(
    public id: number,
    public body: string,
    public image_id: number,
    public listed: string,
    public slug: string,
    public title: string,
    public user_id: number,
    public display_date: string,
    public created_at: string,
    public updated_at: string,
		public url?: string
    ) { }
  }
