export class Banner {
    constructor(
        public id: number,
        public image: string,
        public news_id: number,
        public external_link: string,
        public slug: string
    ) { }
}