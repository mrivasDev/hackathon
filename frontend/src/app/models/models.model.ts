import { Upload } from "./upload.model";

export class Models {
	constructor(
		public description: string,
		public type?: number,
		public id?: number,
		public subject?: string,
		public category?: string,
		public year?: number,
		public upload_id?: number,
		public upload?: Upload,
		public expandedUI?: boolean
	) { }
}
