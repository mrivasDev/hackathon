export class Upload {
  constructor(
    public id: number,
    public created_at?: string,
    public folder?: string,
    public hash?: string,
    public name?: string,
    public is_public?: boolean,
    public preview?:string,
    public type?: string,
    ) { }
  }
