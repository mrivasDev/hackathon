
export class Pagination {
	constructor(
		public data: any[],
		public first_page_url: string | null,
		public last_page_url: string | null,
		public next_page_url: string | null,
		public path: string | null,
		public prev_page_url: string | null,
		public per_page: string | null,
		public current_page: number,
		public from: number,
		public last_page: number,
		public to: number,
		public total: number,
	) { }
}
