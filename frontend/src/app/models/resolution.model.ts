import { Upload } from "./upload.model";

export class Resolution {
    constructor(
        public id?: number,
        public description?: string,
        public subject?: string,
        public year?: number,
        public upload_id?: number,
        public upload?: Upload,
				public expandedUI?: boolean
    ) { }
}
