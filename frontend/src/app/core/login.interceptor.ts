import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { UtilService } from './util.service';
import { MatDialog } from '@angular/material/dialog';

@Injectable()
export class LoginInterceptor implements HttpInterceptor {
	constructor(
		private utilService: UtilService,
		private router: Router,
		private dialog: MatDialog
		) { }
		
		intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
			return next
			.handle(request).pipe(
				tap(() => { }, (errorResponse: any) => {
					if (errorResponse instanceof HttpErrorResponse) {
						if (errorResponse.status === 403) {
							this.utilService.notification('You have no permitions to do that.');
						} else if (errorResponse.status === 401) {
							this.utilService.notification('Your session expired. Log in again');
							this.router.navigateByUrl('/login');
							this.dialog.closeAll();
						}
					}
				}));
			}
		}
		