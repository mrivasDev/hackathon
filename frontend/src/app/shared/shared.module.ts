
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HttpService as HttpServiceComponents, HttpService } from '../core';
import { CourseComponent } from '../components/course/course.component';
import { CpcelpHeaderComponent } from '../components/header/header.component';
import { CpcelpSubtitleComponent } from '../components/header/subtitle.component';
import { CpcelpTitleComponent } from '../components/header/title.component';
import { UnderConstructionComponent } from '../components/under-construction/under-construction.component';

@NgModule({
	declarations: [
		CourseComponent,
		CpcelpHeaderComponent,
		CpcelpSubtitleComponent,
		CpcelpTitleComponent,
		UnderConstructionComponent
	],
	imports: [
		CommonModule,
		MaterialModule,
		FormsModule,
		ReactiveFormsModule,
	],
	exports: [
		CommonModule,
		MaterialModule,
		FormsModule,
		ReactiveFormsModule,
		CourseComponent,
		CpcelpHeaderComponent,
		CpcelpSubtitleComponent,
		CpcelpTitleComponent,
		UnderConstructionComponent
	],
	providers: [
		{ provide: HttpServiceComponents,
			useClass: HttpService
		},
		{ provide: MAT_DIALOG_DATA,
			useValue: {}
		},
		{ provide: MatDialogRef,
			useValue: {}
		}
	]
})

export class SharedModule { }
