import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { DashboardModule } from './pages/dashboard';
import { LoginModule } from './pages/login/login.module';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { WebsiteModule } from './pages/website/website.module';

const routes: Routes = [
	{
		path: 'backoffice',
		canActivate: [AuthGuard],
		loadChildren: () => DashboardModule,
	},
	{
		path: 'login',
		loadChildren:  () => LoginModule
	},
	{
		path: '',
		loadChildren:  () => WebsiteModule
	},
	{
		path: '**',
		component: NotFoundComponent
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
	exports: [RouterModule]
})
export class AppRoutingModule { }
