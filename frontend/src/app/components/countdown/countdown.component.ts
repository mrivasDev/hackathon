import { Component, OnInit, Input } from '@angular/core';
import { interval } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

@Component({
	selector: 'app-countdown',
	templateUrl: './countdown.component.html',
	styleUrls: ['./countdown.component.scss'],
})

export class CountdownComponent implements OnInit {
	@Input() enddate: string;
	@Input() endhour: number;
	@Input() endminute: number;

	public _diff: number;
	public _days: number;
	public _hours: number;
	public _minutes: number;
	public _seconds: number;

	private endDate: any = null;

	constructor() { }

	ngOnInit() {

		const year = parseInt(this.enddate.split('-')[0]);
		const month = parseInt(this.enddate.split('-')[1])-1;
		const day = parseInt(this.enddate.split('-')[2]);

		this.endDate = dayjs(new Date(year, month, day)).hour(this.endhour).minute(this.endminute);
		interval(1000).pipe(
			map((x) => {
				const TODAY = dayjs();
				this._diff = this.endDate.diff(TODAY);
			})).subscribe((x) => {
				this._days = this.getDays(this._diff);
				this._hours = this.getHours(this._diff);
				this._minutes = this.getMinutes(this._diff);
				this._seconds = this.getSeconds(this._diff);
			});
	}

	getDays(t) {
		return Math.floor(t / (1000 * 60 * 60 * 24));
	}

	getHours(t) {
		return Math.floor((t / (1000 * 60 * 60)) % 24);
	}

	getMinutes(t) {
		return Math.floor((t / 1000 / 60) % 60);
	}

	getSeconds(t) {
		return Math.floor((t / 1000) % 60);
	}
}
