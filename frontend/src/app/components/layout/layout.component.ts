import { Component, OnInit, HostListener, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Router } from '@angular/router';
import { UtilService } from '../../core';
import { AuthService } from '../../core/auth.service';
import { Page, Section } from '../../interfaces';
import { User } from '../../models';
import { DashboardService } from '../../pages/dashboard';

@Component({
	selector: 'ad-infosec-layout',
	templateUrl: './layout.component.html',
	styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

	@ViewChild('snav') snav: MatSidenav;

	@ViewChild('confNav') confNav: MatSidenav;

	public lastUrl: string;

	public user: User = this.utilService.getLS('user', true);

	public isDarkTheme: boolean = false;

	public themeColor: 'primary' | 'accent' = 'primary';

	public isMobile: boolean = false;

	public sections: Section[] = [
		{
			title: '',
			children: [

			]
		}
	];

	constructor(
		private dashboardService: DashboardService,
		private utilService: UtilService,
		private router: Router,
		private authService: AuthService
		) {
			this.themeColor = this.utilService.getLS('themeColor', true);
			this.isDarkTheme = this.utilService.getLS('isDarkTheme', true);
			if (this.snav) this.snav.opened = this.dashboardService.isSnavOpened;

			if (this.confNav) this.confNav.opened = this.dashboardService.isConfNavOpened;
		}

		ngOnInit(): void {

			if (document.documentElement.clientWidth <= 750) {
				this.isMobile = true;
				this.dashboardService.isSnavOpened = false;
				this.dashboardService.isConfNavOpened = false;
			}

			this.dashboardService.onSidenavToggle.subscribe(() => {
				if (!this.isMobile) {
					this.snav.opened = this.dashboardService.isSnavOpened;
				}
			});

			this.dashboardService.onConfNavToggle.subscribe(() => {
				this.confNav.opened = this.dashboardService.isConfNavOpened;
			});

			this.dashboardService.onDarkThemeToggle.subscribe(() => {
				this.isDarkTheme = this.dashboardService.isDarkTheme;
			});

			this.dashboardService.onPrimaryThemeSelected.subscribe(() => {
				this.themeColor = 'primary';
			});

			this.dashboardService.onAccentThemeSelected.subscribe(() => {
				this.themeColor = 'accent';
			});

			setTimeout(() => {
				this.lastUrl = this.router.url;
				this.confNav.opened = this.dashboardService.isConfNavOpened;
			});

			setTimeout(() => {
				if (!this.isMobile) {
					this.snav.opened = this.dashboardService.isSnavOpened;
				}
			});

			this.renderLinks();
		}

		renderLinks() {
			this.authService.getPermissions().then((permissions: string) => {
				this.sections[0].children.push(
					(permissions.includes('edit banners') && {
						title: 'Banners',
						icon: 'burst_mode',
						url: '/backoffice/banners'
					}),
					(permissions.includes('edit pages') && { url: '/backoffice/pages', title: 'Webpages', icon: 'web' }),
					(permissions.includes('edit news') && {
						title: 'News',
						icon: 'library_books',
						url: '/backoffice/news'
					}),
					(permissions.includes('edit documents') && { url: '/backoffice/documents', title: 'Documents', icon: 'assignment' }),
					),
					this.sections[0].children = this.sections[0].children.filter(n => n);
				}).catch(console.log);
			}

			navigate(page) {
				if (page && page.url.indexOf("https") !== -1) {
					window.open(page.url, "_blank");
				} else {
					this.router.navigateByUrl(page.url);
					this.lastUrl = page.url;
					this.scrollToTop();
				}
			}

			scrollToTop() {
				const c = document.documentElement.scrollTop || document.body.scrollTop;
				if (c > 0) {
					window.requestAnimationFrame(() => {
						this.scrollToTop();
					});
					window.scrollTo(0, c - c / 8);
				}
			}

			isActive(page: Page) {
				return this.router.isActive(page.url, false);
			}

			isSettingButton({ icon }) {
				return !this.isMobile && icon === 'settings';
			}

			getMode() {
				return !this.isMobile ? 'side' : 'over';
			}

			@HostListener('window:resize', ['$event'])
			onResize(event) {
				this.isMobile = event.target.innerWidth < 750;
				if (this.snav) this.snav.opened = this.dashboardService.isSnavOpened;
			}
		}
