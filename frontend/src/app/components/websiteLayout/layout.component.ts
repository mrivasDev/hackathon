import { Component, OnInit, HostListener, ViewChild } from '@angular/core';


@Component({
	selector: 'website-layout',
	templateUrl: './layout.component.html',
	styleUrls: ['./layout.component.scss']
})
export class WebsiteLayoutComponent implements OnInit {

	public isMobile: boolean = false;

	constructor(){}

	ngOnInit(): void {}


	@HostListener('window:resize', ['$event'])
	onResize(event) {
		this.isMobile = event.target.innerWidth < 750;

	}
}
