import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ad-infosec-title',
  template: '<ng-content></ng-content>'
})
export class CpcelpTitleComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
