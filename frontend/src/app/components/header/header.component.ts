import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ad-infosec-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class CpcelpHeaderComponent implements OnInit {

  @Input() noPadding = false;
  @Input() color = '#333';

  constructor() { }

  ngOnInit() { }

  public getStyle() {
    return {
      'padding-bottom.px': this.noPadding ? '0' : '25',
      color: this.color
    };
  }
}
