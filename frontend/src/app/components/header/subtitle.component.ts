import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ad-infosec-subtitle',
  template: '<ng-content></ng-content>',
})
export class CpcelpSubtitleComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
