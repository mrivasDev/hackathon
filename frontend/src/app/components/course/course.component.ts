import { Component, Input, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { UtilService } from '../../core';
import { Training } from '../../models/training.model';

@Component({
	selector: 'ad-infosec-course',
	templateUrl: './course.component.html',
	styleUrls: ['./course.component.scss'],
})
export class CourseComponent {
	@Input() mobile: boolean = false;

	@Input() course: Training = null;

	@Input() canEdit: boolean = false;

	@Output() clickMainAction = new EventEmitter<Training>();

	@Output() clickSecondaryAction = new EventEmitter<Training>();

	constructor(private utilService: UtilService, private router: Router) { }

	onMainClickHandler(): void {
		this.clickMainAction.emit(this.course);
	}

	onSecondaryClickHandler(): void {
		this.clickSecondaryAction.emit(this.course);
	}

	parseDate(date: string, time: string) {
		let ret = '';
		const hour = parseInt(time.split(':')[0]);
		const minute = parseInt(time.split(':')[1]);
		if (this.utilService.isDateBefore(date, hour, minute)) {
			ret = this.utilService.getDateToNow(date, hour, minute);
			ret = `hace ${ret}`;
		} else {
			ret = this.utilService.getDateFromNow(date, hour, minute);
		}

		return ret;
	}

	goToDetails(): void {
		this.router.navigate(['/capacitaciones/detalle/', this.course.id]);

	}

}
