import { OverlayContainer } from '@angular/cdk/overlay';
import { Component, OnInit } from '@angular/core';
import { MatRadioChange } from '@angular/material/radio';
import { UtilService } from '../../core/util.service';
import { DashboardService } from '../../pages/dashboard/dashboard.service';

enum ThemeColor {
  primary = 'primary',
  accent = 'accent'
}

@Component({
  selector: 'ad-infosec-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})

export class SettingsComponent implements OnInit {

  themeColor: ThemeColor = ThemeColor.primary;

  isDarkTheme: boolean = false;

  themeList: string[] = ['primary', 'accent'];

  constructor(
    private dashboardService: DashboardService,
    private utilService: UtilService,
    private overlayContainer: OverlayContainer
  ) {
    this.themeColor = this.utilService.getLS('themeColor', true);
    this.isDarkTheme = this.utilService.getLS('isDarkTheme', true);
  }

  ngOnInit(): void {

    this.dashboardService.onDarkThemeToggle.subscribe(() => {
      this.isDarkTheme = this.dashboardService.isDarkTheme;
    });

    this.dashboardService.onPrimaryThemeSelected.subscribe(() => {
      this.themeColor = ThemeColor.primary;
    });

    this.dashboardService.onAccentThemeSelected.subscribe(() => {
      this.themeColor = ThemeColor.accent;
    });

  }

  themeChange(event: MatRadioChange): void {
    switch (event.value) {
      case 'primary': this.selectPrimaryTheme(); break;
      case 'accent': this.selectAccentTheme(); break;
    }
  }

  toggleMode(): void {
    this.toggleTheme();
  }

  selectPrimaryTheme(): void {
    this.dashboardService.onPrimaryThemeSelected.emit();
  }

  selectAccentTheme(): void {
    this.dashboardService.onAccentThemeSelected.emit();
  }

  toggleTheme(): void {
    this.dashboardService.onDarkThemeToggle.emit();
    if (this.isDarkTheme) {
      this.overlayContainer.getContainerElement().classList.add('dark-theme');
    } else {
      this.overlayContainer
        .getContainerElement()
        .classList.remove('dark-theme');
    }
  }

  getColor(): ThemeColor {
    switch (this.themeColor) {
      case 'primary': return ThemeColor.primary;
      case 'accent': return ThemeColor.accent;
    }
  }

  getThemeLabel(themeColor: string): string {
    switch (themeColor) {
      case 'primary': return this.isDarkTheme ? 'Primary Dark' : 'Primary'
      case 'accent': return this.isDarkTheme ? 'Secondary Dark' : 'Secondary';
    }
  }

}
