import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopbarWebsiteComponent } from './topbar-website.component';

describe('TopbarWebsiteComponent', () => {
  let component: TopbarWebsiteComponent;
  let fixture: ComponentFixture<TopbarWebsiteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopbarWebsiteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopbarWebsiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
