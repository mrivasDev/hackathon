import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'ad-infosec-topbar-website',
	templateUrl: './topbar-website.component.html',
	styleUrls: ['./topbar-website.component.scss']
})
export class TopbarWebsiteComponent implements OnInit {

	public themeColor: 'primary' | 'accent' = 'primary';

	constructor(private router: Router) { }

	ngOnInit(): void {
	}

	public navigate(page: string = '') {
		this.router.navigateByUrl(page);
	}

}










