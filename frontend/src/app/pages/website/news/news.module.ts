import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewsComponent } from './news.component';
import { RouterModule } from '@angular/router';
import { NewsResolver } from './news.resolver';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [NewsComponent],
  imports: [
    CommonModule,
		MatButtonModule,
		RouterModule.forChild([
			{
				path: ':slug', component: NewsComponent,
				resolve: {
					page: NewsResolver
				}
			},
		])
  ],
	providers: [
		NewsResolver
	],
	exports: [
		RouterModule
	]
})
export class NewsModule { }
