import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Article } from '../../../models';

@Component({
	selector: 'ad-infosec-website-news',
	templateUrl: './news.component.html',
	styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnDestroy {
	public sub: any;
	public slug = "";
	public article: Article;
	public dataLoaded = false;
	constructor(private activatedRoute: ActivatedRoute, private router: Router) {
		console.log(this.activatedRoute.data)
		this.sub = this.activatedRoute.data.subscribe(({ page }) => {
			this.article = page;
			this.dataLoaded = true;
			console.log(page)
		});
	}

	public getBanner() {
		return {
			backgroundImage: this.article.image
			? `url(${this.article.image})`
			: 'url("/assets/img/calm.jpg")',
			backgroundPosition: "center",
			backgroundSize: "cover",
		};
	}

	public goHome(): void {
		this.router.navigate(['/']);
	}

	ngOnDestroy() {
		this.sub.unsubscribe();
	}
}
