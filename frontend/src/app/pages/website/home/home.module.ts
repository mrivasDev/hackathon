import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { RouterModule } from '@angular/router';
import { MatCarouselModule } from '@ngbmodule/material-carousel';
import { MatGridListModule } from '@angular/material/grid-list';
import { LoadingModule } from 'projects/ng-utils/src/lib/loading/loading.module';
import { MatRippleModule } from '@angular/material/core';
import { MatCardModule } from '@angular/material/card';

@NgModule({
	declarations: [HomeComponent],
	imports: [
		CommonModule,
		MatCarouselModule,
		MatGridListModule,
		LoadingModule,
		MatRippleModule,
		MatCardModule,
		RouterModule.forChild([{ path: '', component: HomeComponent }])
	]
})
export class HomeModule { }
