import { HostListener } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UtilService, HttpService } from '../../../core/';
import { Banner, News } from '../../../models';

@Component({
	selector: 'ad-infosec-website-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

	public isMobile: boolean = false;
	public loadingBanners: boolean = false;

	public loadingNews: boolean = false;

	public banners: Banner[] = [];

	public news: News[] = [];

	constructor( private utilService: UtilService, private httpService: HttpService, private router: Router ) { }

	ngOnInit(): void {
		this.isMobile = document.documentElement.clientWidth <= 750;
		this.fetchBanners();
		this.fetchNews();
	}

	private fetchBanners() {
		this.loadingBanners = true;
		this.httpService.get('website/news').then((news: any) => {
			this.news = news;
			this.loadingBanners = false;
		});
	}

	private fetchNews() {
		this.loadingNews = true;
		this.httpService.get('website/banners').then((banners: Banner[]) => {
			this.banners = banners;
			this.loadingNews = false;
		});
	}

	public trackByBanner(index: number, banner: any): string {
		return banner ? `banner-${index}` : null;
	}

	public goToNews(slug: string): void {
		this.router.navigateByUrl(`/news/${slug}`);
	}

	@HostListener('window:resize', ['$event'])
	onResize(event): void {
		this.isMobile = event.target.innerWidth < 750;
	}

}
