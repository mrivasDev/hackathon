import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PageMock } from './mock';

@Component({
	selector: 'ad-infosec-website-page',
	templateUrl: './page.component.html',
	styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnDestroy {
	sub: any;
	slug = "";
	pageMock = PageMock;
	page: any;
	dataLoaded = false;
	constructor(private activatedRoute: ActivatedRoute, private router: Router) {
		this.sub = this.activatedRoute.data.subscribe(({ page }) => {
			this.page = page;
			this.dataLoaded = true;
		});
	}

	public getBanner() {
		return {
			backgroundImage: this.page.url
			? `url(${this.page.url})`
			: 'url("/assets/img/calm.jpg")',
			backgroundPosition: "center",
			backgroundSize: "cover",
		};
	}

	public goHome(): void {
		this.router.navigate(['/']);
	}

	ngOnDestroy() {
		this.sub.unsubscribe();
	}
}
