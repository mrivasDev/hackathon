import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageComponent } from './page.component';
import { RouterModule } from '@angular/router';
import { PageResolver } from './page.resolver';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
	declarations: [PageComponent],
	imports: [
		CommonModule,
		MatButtonModule,
		RouterModule.forChild([
			{
				path: ':slug', component: PageComponent,
				resolve: {
					page: PageResolver
				}
			},
		])
	],
	providers: [
		PageResolver
	],
	exports: [
		RouterModule
	]
})
export class PageModule { }
