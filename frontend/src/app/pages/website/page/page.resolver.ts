import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { HttpService } from 'src/app/core/http.service';
import { Page } from '../../../interfaces';


@Injectable({
	providedIn: 'root'
})
export class PageResolver implements Resolve<any> {

	constructor(
		private router: Router,
		private httpService: HttpService,

	) { }

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<any> {
		return new Promise((resolve, reject) => {
			const slug = route.params['slug'] ? route.params['slug'] : null;
			if (slug) {
				return this.httpService.get('website/page/' + slug).then((page: Page) => {
					resolve(page);
				}).catch(error => {
					console.error(error);
					this.router.navigateByUrl('/');
					reject();
				});
			} else {
				reject();
			}
		});
	}

}


