import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeModule } from './home/home.module';
import { NewsModule } from './news/news.module';
import { PageModule } from './page/page.module';
import { WebsiteComponent } from './website.component';

const routes: Routes = [
	{
		path: '',
		component: WebsiteComponent,
		children: [
			{
				path: '',
				redirectTo: 'home',
				pathMatch: 'full'
			},
			{
				path: 'home',
				loadChildren: () => HomeModule,
			},
			{
				path: 'news',
				loadChildren: () => NewsModule,
			},
			{
				path: 'pages',
				loadChildren: () => PageModule,
			},
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class WebsiteRoutingModule { }
