import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebsiteComponent } from './website.component';
import { WebsiteLayoutComponent } from '../../components/websiteLayout/layout.component';
import { FooterComponent } from '../../components/footer/footer.component';
import { TopbarWebsiteComponent } from '../../components/topbar-website/topbar-website.component';
import { WebsiteRoutingModule } from './website-routing.module';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
	declarations: [
		WebsiteComponent,
		FooterComponent,
		TopbarWebsiteComponent,
		WebsiteLayoutComponent
	],
	imports: [
		CommonModule,
		MatToolbarModule,
		FlexLayoutModule,
		MatMenuModule,
		MatButtonModule,
		MatIconModule,
		MatDividerModule,
		MatToolbarModule,
		MatListModule,
		MatButtonModule,
		WebsiteRoutingModule
	]
})
export class WebsiteModule { }
