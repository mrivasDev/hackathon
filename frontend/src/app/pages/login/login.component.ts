import { Component, HostListener, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilService } from '../../core'
import { AuthService } from '../../core/auth.service';


@Component({
	selector: 'ad-infosec-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

	public slowNetwork: boolean = false;
	public noConection: boolean = false;
	public loading: boolean = false;

	public isMobile: boolean = false;

	public loginForm = new FormGroup({
		username: new FormControl(null),
		password: new FormControl(null),
	});

	public showPass: boolean = false;

	public alert = { visible: false };

	private redirectUrl = undefined;

	private idParam = null;

	public title: string = 'AD Security Web Page';

	constructor(private router: Router, private activatedRoute: ActivatedRoute, private utilService: UtilService, private authService: AuthService) { }

	ngOnInit(): void {
		this.isMobile = document.documentElement.clientWidth <= 750;

		this.activatedRoute.queryParams.subscribe((params) => {
			this.redirectUrl = params["redirectUrl"];
			this.idParam = params["id"];
		});
	}

	public reset() {
		this.loginForm.reset();
	}

	public loginDisabled() {
		return !this.loginForm.value.username || !this.loginForm.value.password || this.loading
	}

	public login() {
		const login = this.loginForm.value;
		this.loading = true;
		this.loginForm.controls['username'].disable();
		this.loginForm.controls['password'].disable();
		const notification = this.utilService.notification( "Login in...", "", 5 );

		setTimeout(() => {
			if (this.loading === true) {
				if (window.navigator.onLine) {
					this.slowNetwork = true;
				} else {
					this.noConection = true;
				}
			}
		}, 5000, this.loading);

		this.authService
		.login(login.username, login.password)
		.then((data) => {
			if (data && data.user) {
				this.utilService.setLS("user", data.user, true);
				this.utilService.saveAuth(data);
				notification.dismiss();
				this.loading = false;
				this.slowNetwork = false;
				this.noConection = false;
				const redirection = this.redirectUrl ? `/backoffice/${this.redirectUrl}${this.idParam ? `/${this.idParam}` : ''}` : "/backoffice";
				window.location.href = redirection;
			}
		})
		.catch((error) => {
			this.loading = false;
			this.slowNetwork = false;
			this.loginForm.controls['username'].enable();
			this.loginForm.controls['password'].enable();
			if (window.navigator.onLine) {
				this.noConection = false;
				if (error.message === "Wrong credentials") {
					this.utilService.notification(error.message);
					this.resetPassword();
				}
			} else {
				this.noConection = true;
			}
		});
	}

	public	resetPassword() {
		this.alert.visible = true;
	}

	public hideAlert() {
		this.alert.visible = false;
	}

	public getBackground() {
		var date = new Date();
		const currentTime = date.getHours();
		if (!this.isMobile){
			const imageUrl = `assets/img/login/background.jpeg`
			return { backgroundImage: `url("${imageUrl}")` }
		}
		const mobileBackground = `assets/img/login/background.jpeg`
		return { background: `${mobileBackground}` };
	}

	@HostListener('window:resize', ['$event'])
	onResize(event): void {
		this.isMobile = event.target.innerWidth < 750;
	}

}
