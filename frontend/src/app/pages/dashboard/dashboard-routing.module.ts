import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BannersModule } from './banners/banners.module';
import { DashboardComponent } from './dashboard.component';
import { HomeModule } from './home/home.module';
import { NewsModule } from './news/news.module';
import { TechnicalSecretaryModule } from './technical-secretary/technical-secretary.module';
import { UsersModule } from './users/users.module';
import { WebpagesModule } from './webpages/webpages.module';
import { RoleGuard } from './role.guard';

const routes: Routes = [
	{
		path: '',
		component: DashboardComponent,
		children: [
			{
				path: '',
				redirectTo: 'banners',
				pathMatch: 'full'
			},
			{
				path: 'news',
				loadChildren: () => NewsModule,
				canActivate: [RoleGuard],
				data: {
					permissions: [
						'edit news'
					]
				}
			},
			{
				path: 'pages',
				loadChildren: () => WebpagesModule,
				canActivate: [RoleGuard],
				data: {
					permissions: [
						'edit pages'
					]
				}
			},
			{
				path: 'banners',
				loadChildren: () => BannersModule,
				canActivate: [RoleGuard],
				data: {
					permissions: [
						'edit banners'
					]
				}
			},
			{
				path: 'usuarios',
				loadChildren: () => UsersModule,
				canActivate: [RoleGuard],
				data: {
					permissions: [
						'gestionar usuarios'
					]
				}
			}
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class DashboardRoutingModule { }
