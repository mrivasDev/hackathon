import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { AuthService } from '../../core/auth.service';

@Injectable({
	providedIn: 'root'
})
export class FeatureRoleGuard implements CanActivate {

	constructor(
		private authService: AuthService,
	) { }
	async canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		const user = await this.authService.identity();
		if (next.data.user.some(someUser => someUser.email === user.email)) {
			return (true)
		} else {
			return (false);
		}

	}
}
