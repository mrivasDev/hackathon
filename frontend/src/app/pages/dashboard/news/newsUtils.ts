import { Validators } from "@angular/forms";
import { FormLayout } from "projects/ng-utils/src/lib/header/header.component";

export interface NewsColumn {
	field: string, display: string
}

export interface TableSettings {
	pageSize: number,
	pageIndex: number,
	resultsLength: number,
	pageSizeOptions: number[]
}

export const modalFormElements: FormLayout[] = [
	{
		rowIndex: 1, element:
		[{
			field: 'image_id',
			display: "Picture",
			placeholder: "Banner picture",
			type: "image",
			element: 'image'
		}]
	},
	{
		rowIndex: 2, element:
		[{
			field: 'title',
			display: "Title",
			class: 'w-100',
			placeholder: "Title",
			validator: [Validators.required, Validators.minLength(8)],
			type: "text",
			element: 'input'
		}]
	},
	{ rowIndex: 3, element:
		[{
			field: 'body',
			display: "wysiwyg",
			placeholder: "wysiwyg",
			type: "text",
			element: 'wysiwyg'
		}]
	}
];

export const columnsToDisplay = [
	{ field: "title", display: "Title" },
	{ field: "image", display: "Picture" },
	{ field: "actions", display: "Actions" }
];
