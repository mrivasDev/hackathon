import { Component, OnInit, HostListener } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { FormLayout } from 'projects/ng-utils/src/lib/header/header.component';
import { ConfirmationMobileComponent } from 'projects/ng-utils/src/lib/confirmation/confirmationBottomSheet/confirmation.component';
import { ConfirmationDesktopComponent } from 'projects/ng-utils/src/lib/confirmation/confirmationModal/confirmation.component';
import { MainActionBottomSheetComponent } from 'projects/ng-utils/src/lib/mainActionBottomSheet/mainActionBottomSheet.component';
import { MainActionModalComponent } from 'projects/ng-utils/src/lib/mainActionModal/mainActionModal.component';
import { ActionTable } from 'projects/ng-utils/src/lib/table/table.component';
import { HttpService } from '../../../core/http.service';
import { NewsColumn, columnsToDisplay, modalFormElements, TableSettings } from './newsUtils';
import { News } from '../../../models/news.model';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { environment } from '../../../../environments/environment';

@Component({
	selector: 'ad-infosec-news',
	templateUrl: './news.component.html',
	styleUrls: ['./news.component.scss']
})

export class NewsComponent implements OnInit {
	public isMobile: boolean = false;

	public dataSource: any[] = [];


	public isLoading: boolean = false;

	public columnsToDisplay: NewsColumn[] = columnsToDisplay;

	public modalTitle: string = 'New';

	public modalSubTitle: string = 'Create news';

	public modalApiUrl: string = 'news';

	public modalForm: FormLayout[] = modalFormElements;

  public noNewsIcon : string = 'sentiment_very_dissatisfied';
  public noNewsMessage : string = 'Looks like there is no news, you can create them by clicking on "New"';

	public angularEditorConfig: AngularEditorConfig = {
		editable: true,
		spellcheck: true,
		minHeight: "calc(50vh - 200px)",
		maxHeight: "calc(50vh - 100px)",
		placeholder: "Insert here the news body",
		translate: "no",
		sanitize: false,
		toolbarPosition: "top",
		defaultFontName: "Arial",
		customClasses: [],
	};

	public dialogAngularEditorConfig: AngularEditorConfig = {
		editable: true,
		spellcheck: true,
		minHeight: "calc(50vh - 200px)",
		maxHeight: "calc(50vh - 100px)",
		placeholder: "Insert here the news body",
		translate: "no",
		sanitize: false,
		toolbarPosition: "top",
		defaultFontName: "Arial",
		customClasses: [],
	};

	public tableSettings: TableSettings = {
		pageSize: 0,
		pageIndex: 0,
		resultsLength: 0,
		pageSizeOptions: [15]
	}

	public tableActions: ActionTable[] = [
		{
			icon: 'visibility',
			name: 'Ver',
			callback: (element) => { this.handleOpen(element) },
		},
		{
			icon: 'edit',
			name: 'Edit',
			callback: (element) => { this.handleEdit(element) },
		},
		{
			icon: 'delete',
			name: 'Delete',
			callback: (element) => { this.handleDelete(element) },
		},
	];

	constructor(public dialog: MatDialog, public bottomSheet: MatBottomSheet,	private httpService: HttpService) { }

	ngOnInit(): void {
		this.isMobile = document.documentElement.clientWidth <= 750;
		this.fetchNews();
	}

	public fetchNews(searchText: string | number = ""): void {
		this.isLoading = true;
		this.httpService.get('news', { size: this.tableSettings.pageSize, page: this.tableSettings.pageIndex, searchText }).then(({
			per_page,
			current_page,
			total,
			data
		}: any) => {
			this.tableSettings.pageSize = per_page;
			this.tableSettings.pageIndex = current_page - 1;
			this.tableSettings.resultsLength = total;
			this.dataSource = this.parseData(data);
			this.isLoading = false;
		});
	}

	public handlePageEvent(event: PageEvent) {
		if (event.pageSize !== this.tableSettings.pageSize) {
			this.tableSettings.pageSize = event.pageSize;
		}

		if (event.pageIndex !== this.tableSettings.pageIndex) {
			this.tableSettings.pageIndex = event.pageIndex;
		}

		this.fetchNews();
	}

	public handleFinishCreating(event: any) {
		this.fetchNews();
	}

	public handleOpen({ slug }: News){
		window.open(`http://localhost:4200/news/${slug}`, '_blank');
	}

	public handleEdit(article: News) {
		const parsedArticle = {
			...article,
			...{
				display_date: article.display_date === "Mostrar" ? true : false,
				listed: article.listed === "Listada" ? true : false,
				image: article.url
			}
		}
		if (this.isMobile) {
			this.openEditBottomSheet(parsedArticle);
		} else {
			this.openEditDialog(parsedArticle);
		}
	}

	public openEditDialog(article: any) {
		const title = "Edit news";
		const subtitle = `${article.title}`;
		const editDialog = this.dialog.open(MainActionModalComponent, {
			width: '650px',
			height: 'auto',
			data: {
				title,
				subtitle,
				apiUrl: `${this.modalApiUrl}`,
				form: this.modalForm,
				method: 'post',
				element: article,
				angularEditorConfig: this.dialogAngularEditorConfig,
				saveLabel: 'Save'
			}
		});
		editDialog.afterClosed().subscribe(result => {
			this.fetchNews();
		});
	}

	public openEditBottomSheet(mobileArticle: any) {
		const foundedArticle = this.dataSource.find((news: News) => news.id === mobileArticle.id);
		const article =  {
			...foundedArticle,
			...{
				image: foundedArticle.url
			}
		}
		const title = "Edit news";
		const subtitle = `${article.title}`;
		const editBottomSheet = this.bottomSheet.open(MainActionBottomSheetComponent, {
			data: {
				title,
				subtitle,
				apiUrl: `${this.modalApiUrl}`,
				form: this.modalForm,
				method: 'post',
				element: article,
				angularEditorConfig: this.dialogAngularEditorConfig,
				saveLabel: 'Save'
			}
		});
		editBottomSheet.afterDismissed().subscribe(() => {
			this.fetchNews();
		});
	}

	public handleDelete(article: News) {
		const parsedArticle = {
			...article,
			...{
				display_date: article.display_date === "Mostrar" ? true : false,
				listed: article.listed === "Listada" ? true : false,
				image: article.url
			}
		}
		if (this.isMobile) {
			this.openDeleteBottomSheet(article);
		} else {
			this.openDeleteDialog(article);
		}
	}

	public openDeleteDialog(article: any) {
		const title = "Delete news";
		const subtitle = `${article.title}`;
		const editDialog = this.dialog.open(ConfirmationDesktopComponent, {
			width: '650px',
			height: 'auto',
			data: {
				title,
				subtitle,
				apiUrl: `${this.modalApiUrl}/delete/${article.id}`,
				form: this.modalForm,
				method: 'post',
				element: article,
				saveLabel: 'Delete'
			}
		});
		editDialog.afterClosed().subscribe(result => {
			this.fetchNews();
		});
	}

	public openDeleteBottomSheet(mobileArticle: any) {
		const foundedArticle = this.dataSource.find((news: News) => news.id === mobileArticle.id);
		const article =  {
			...foundedArticle,
			...{
				image: foundedArticle.url
			}
		}
		const title = "Delete news";
		const subtitle = `${article.title}`;
		const deleteBottomSheet = this.bottomSheet.open(ConfirmationMobileComponent, {
			data: {
				title,
				subtitle,
				apiUrl: `${this.modalApiUrl}/delete/${article.id}`,
				form: this.modalForm,
				method: 'post',
				element: article,
				saveLabel: 'Delete'
			}
		});
		deleteBottomSheet.afterDismissed().subscribe(() => {
			this.fetchNews();
		});
	}

	public parseData(news: News[]){
		news = news.map((eachNews): News => {
			eachNews.display_date = eachNews.display_date ? 'Show' : 'Do not show';
			eachNews.listed = eachNews.listed ? 'Listed' : 'Non listed';
			return eachNews;
		})
		return news;
	}

	public trackNews({id}: News): number {
		return id;
	}

	@HostListener('window:resize', ['$event'])
	onResize(event): void {
		this.isMobile = event.target.innerWidth < 750;
	}

}
