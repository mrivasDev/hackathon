import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from 'projects/ng-utils/src/lib/header/header.module';
import { LoadingModule } from 'projects/ng-utils/src/lib/loading/loading.module';
import { SegmentModule } from 'projects/ng-utils/src/lib/segment/segment.module';
import { TableModule } from 'projects/ng-utils/src/lib/table/table.module';
import { NewsComponent } from './news.component';
import { RouterModule } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatDividerModule } from '@angular/material/divider';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { ConfirmationDesktopModule } from 'projects/ng-utils/src/lib/confirmation/confirmationModal/confirmation.module';
import { ConfirmationMobileModule } from 'projects/ng-utils/src/lib/confirmation/confirmationBottomSheet/confirmation.module';

@NgModule({
  declarations: [ NewsComponent ],
  imports: [
    CommonModule,
    HeaderModule,
    SegmentModule,
    TableModule,
    MatListModule,
    MatIconModule,
    LoadingModule,
		MatDividerModule,
		MatMenuModule,
		MatButtonModule,
		MatIconModule,
		ConfirmationDesktopModule,
		ConfirmationMobileModule,
    RouterModule.forChild([{ path: '', component: NewsComponent }])
  ]
})
export class NewsModule { }
