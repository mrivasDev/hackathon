import { Component, OnInit, HostListener } from '@angular/core';
import { FlatTreeControl } from '@angular/cdk/tree';
import { Pages } from './hardcodedpages';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { Router } from '@angular/router';
import { DashboardService } from '../dashboard.service';

interface PageNode {
  id?: number;
  name: string;
  children?: PageNode[];
  slug?: string;
}

const TREE_DATA: PageNode[] = Pages;

/** Flat node with expandable and level information */
interface PageFlatNode {
  id: number;
  name: string;
  level: number;
  expandable: boolean;
}
@Component({
  selector: 'ad-infosec-webpages',
  templateUrl: './webpages.component.html',
  styleUrls: ['./webpages.component.scss']
})
export class WebpagesComponent implements OnInit {
  public isMobile: boolean = false;

  private editing = false;

  treeControl = new FlatTreeControl<PageFlatNode>(node => node.level, node => node.expandable);

  private transformer = (node: PageNode, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      id: node.id,
      level: level,
      name: node.name,
    };
  }

  treeFlattener = new MatTreeFlattener(this.transformer, node => node.level, node => node.expandable, node => node.children);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);


  constructor( private router: Router, private dashboardService: DashboardService ) { this.dataSource.data = TREE_DATA; }

  hasChild = (_: number, node: PageFlatNode) => node.expandable;

  ngOnInit(): void {
    this.isMobile = document.documentElement.clientWidth <= 750;
  }

  public edit(page: any) {
    this.editing = true;
    this.router.navigate(['/backoffice/pages', page.id]);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event): void {
    this.isMobile = event.target.innerWidth < 750;
  }
}
