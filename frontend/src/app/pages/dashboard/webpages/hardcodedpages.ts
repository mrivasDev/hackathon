export const Pages = [
  {name: 'Home'},
  {
    name: 'Overview',
    children: [
      {id: 1, name: 'Marketplace security baseline'},
      {id: 2, name: 'MFA for Marketplace'}
    ]
  },
  {
    name: 'Compliance',
    children: [
      {id: 3, name: 'GDPR (General Data Protection Regulation)'},
      {id: 4, name: 'Certifications'},
    ]
  },
  {
    id: 5, name: 'Security Advisors',
  },
  {name: 'Contact'}
];
