import { Validators } from "@angular/forms";
import { FormLayout } from "projects/ng-utils/src/lib/header/header.component";

export interface WebPageColumn {
  field: string, display: string
}

export const modalFormElements: FormLayout[] = [
  {
    rowIndex: 1, element: [
      { field: 'image_id', display: "Imagen", placeholder: "Imagen del banner", type: "image", element: 'image' },
    ]
  },
  {
    rowIndex: 2, element: [
      { field: 'title', display: "Titulo", placeholder: "Titulo",validator: [Validators.required, Validators.minLength(8)], type: "text", element: 'input' },
    ]
  },
  { rowIndex: 3, element: [
    { field: 'body', display: "wysiwyg", placeholder: "wysiwyg", type: "text", element: 'wysiwyg' }
  ]},
]
