import { Component, OnInit, Inject } from "@angular/core";
import { Validators, FormGroup, FormBuilder } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { AngularEditorConfig } from "@kolkov/angular-editor";

export interface Element {
  id: number;
  title: string;
  body: string;
  page_id: string;
  image_id: string;
  url: string;
}

@Component({
  selector: "pages-new-element-dialog",
  templateUrl: "./new-element-dialog.component.html",
  styleUrls: ["./new-element-dialog.component.scss"],
})
export class NewElementDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<NewElementDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder
  ) {
    this.page_id = data.page_id;
    this.card = data.card;
  }

  card: Element = null;
  portada: string = null;
  page_id: string = null;
  preview: string = null;


  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    minHeight: "calc(50vh - 200px)",
    maxHeight: "calc(50vh - 100px)",
    placeholder: "Drop here the content",
    translate: "no",
    sanitize: false,
    toolbarPosition: "top",
    defaultFontName: "Arial",
    customClasses: [],
  };

  fileMessages = {
    openFile: "Abrir portada",
    noFile: "No hay portada",
    uploadedFile: "Portada subida",
    uploadFile: "Upload banner",
  };

  newElementForm: FormGroup = this.fb.group({
    id: [null],
    title: [null, [Validators.required, Validators.minLength(8)]],
    image_id: [null, Validators.required],
    body: [null, [Validators.required]],
  });

  ngOnInit() {
    this.dialogRef.updateSize("80%", "80%");
    // this.imageUploader.previewChange.subscribe((preview) => {
    //   this.portada = preview;
    // });
    if (this.card) {
      this.newElementForm.patchValue({
        id: this.card.id,
        title: this.card.title,
        body: this.card.body,
      });
      this.portada = this.card.url;
    }
  }

  handleLinkPreview(urlPreview: string) {
    this.preview = urlPreview;
    this.newElementForm.value.url = this.preview;
  }

  onClose() {
    const retValue = this.newElementForm.value.title
      ? {
          ...this.newElementForm.value,
          ...{
            url: this.preview
              ? this.preview
              : this.card.url,
            image_id: this.newElementForm.value.image_id
              ? this.newElementForm.value.image_id
              : this.card.image_id,
          },
        }
      : {};
    this.dialogRef.close(retValue);
  }
}
