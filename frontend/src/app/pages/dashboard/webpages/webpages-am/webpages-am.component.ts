import { Component, OnInit, ViewChild, HostListener } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AngularEditorConfig } from "@kolkov/angular-editor";
import { ActivatedRoute } from "@angular/router";
import { UtilService, HttpService } from "../../../../core";
import { MatDialog } from "@angular/material/dialog";
import { MatTabGroup } from "@angular/material/tabs";
import { ImageUploaderComponent } from "projects/ng-utils/src/lib/image-uploader/image-uploader.component";
import { MatBottomSheet } from "@angular/material/bottom-sheet";
import { MainActionBottomSheetComponent } from "projects/ng-utils/src/lib/mainActionBottomSheet/mainActionBottomSheet.component";
import { MainActionModalComponent } from "projects/ng-utils/src/lib/mainActionModal/mainActionModal.component";
import { FormLayout } from "projects/ng-utils/src/lib/header/header.component";
import { modalFormElements } from "./webPageAmUtils";

export interface Page {
  id?: number;
  name: string;
  children?: Page[];
  slug?: string;
}

export interface PageBasic {
  id: string;
  body: string;
  slug: string;
  title: string;
  url: string | null;
  cards: PageCard[];
  type: "basic" | "grid";
  image_id: string | null;
  created_at: string;
  updated_at: string;
}

export interface PageCard {
  id: number;
  title: string;
  body: string;
  page_id: string;
  image_id: string;
  created_at: string;
  updated_at: string;
  url: string;
  preview?: string;
  image?:string;
}

type EditMode = "basic" | "grid";

@Component({
  selector: "ad-infosec-webpages-am",
  templateUrl: "./webpages-am.component.html",
  styleUrls: ["./webpages-am.component.scss"],
})
export class WebpagesAmComponent implements OnInit {

  @ViewChild("webpageTab", { static: false }) webpageTab: MatTabGroup;
  @ViewChild(ImageUploaderComponent) imageUploaderComponent: ImageUploaderComponent ;

  public NO_BANNER_TITLE = 'Empty webpage';
  public NO_BANNER_ICON = 'sentiment_very_dissatisfied';
  public NO_BANNER_MESSAGE = 'Looks like there is no there is nothing defined on this webpage, you can create content by clicking on "Edit"';
  public ACTION_LABEL = 'Edit';

  public bannerImage: any = null;

  public isMobile: boolean = false;

  public subtitle: string = "";

  public apiUrl: string = 'paginas';

  private hasImage: boolean = false;

  public editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    minHeight: "5rem",
    maxHeight: "calc(100vh - 350px)",
    placeholder: "Insert here the page body",
    translate: "no",
    sanitize: false,
    toolbarPosition: "top",
    defaultFontName: "Arial",
    customClasses: [],
  };

  public gridList: PageCard[] = [];

  public page: PageBasic;
  public portada: string;

  public pageTitle: string;
  public pageForm: FormGroup;

  public breakpoint: number;
  public type: EditMode = "basic";
  public createModeOn: boolean = false;
  public newItem: string = "";

  public loading: boolean = false;

  public modalForm: FormLayout[] = modalFormElements;

  public dialogAngularEditorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    minHeight: "calc(50vh - 200px)",
    maxHeight: "calc(50vh - 100px)",
    placeholder: "Insert here the page body",
    translate: "no",
    sanitize: false,
    toolbarPosition: "top",
    defaultFontName: "Arial",
    customClasses: [],
  };

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private utilService: UtilService,
    private httpService: HttpService,
    private newGridElementMobile: MatBottomSheet,
    private newGridElementDesktop: MatDialog,
    public dialog: MatDialog
    ) {
      this.buildForm();
      this.page = this.route.snapshot.data.page;
      this.subtitle = `Editing ${this.page.title}`;
      this.loadPage();
    }

    ngOnInit() {
      this.breakpoint = window.innerWidth <= 400 ? 1 : 4;
      this.isMobile = document.documentElement.clientWidth <= 750;
    }

    public onChangeMode(newMode: EditMode) {
      this.type = newMode;
    }

    public remove(card: PageCard) {
      this.gridList = this.gridList.filter((element) => element !== card);
    }

    public reset() {
      if (this.page.type === "grid") {
        this.gridList = this.page.cards;
      }
    }

    private openBottomSheet(card: PageCard){
      const newGridElementRef = this.newGridElementMobile.open(MainActionBottomSheetComponent, {
        data: {
          title: 'Add new element',
          subtitle: ' ',
          apiUrl: 'paginas-grilla',
          form: this.modalForm,
          element: card,
          angularEditorConfig: this.dialogAngularEditorConfig,
          saveLabel: 'Add',
          method: ''
        }
      });
      newGridElementRef.afterDismissed().subscribe((element: PageCard | null) => {
        if (element) {
          if (element && Object.keys(element).length) {
            if (element.id) {
              this.gridList = this.gridList.map((gridElement) => {
                if (gridElement.id === element.id) {
                  gridElement = { ...gridElement, ...element };
                }
                return gridElement;
              });
            } else {
              this.addToList(element);
            }
          }
        }
      });
    }

    private openDialog(card: PageCard){
      const newGridElementRef = this.newGridElementDesktop.open(MainActionModalComponent, {
        width: '650px',
        height: 'auto',
        data: {
          title: 'Add new element',
          subtitle: ' ',
          apiUrl: 'paginas-grilla',
          form: this.modalForm,
          element: card,
          angularEditorConfig: this.dialogAngularEditorConfig,
          saveLabel: 'Add',
          method: ''
        }
      });

      newGridElementRef.afterClosed().subscribe((element: PageCard | null) => {
        if (element) {
          if (element && Object.keys(element).length) {
            if (element.id) {
              this.gridList = this.gridList.map((gridElement) => {
                if (gridElement.id === element.id) {
                  gridElement = { ...gridElement, ...element };
                }
                return gridElement;
              });
            } else {
              this.addToList(element);
            }
          }
        }
      });
    }

    handleFinishCreating(event: any) {
      // this.fetchBanners();
    }


    public goToEditMode() {
      this.webpageTab.selectedIndex = 1;
    }
    public addNewElement(card: PageCard) {
      if (card && card.url) {
        card.image = card.url;
      }
      if (this.isMobile) {
        this.openBottomSheet(card);
      }else{
        this.openDialog(card);
      }
    }

    public addToList(element: PageCard) {
      const newPageCard = this.gridList.concat(element);
      this.gridList = newPageCard;
    }

    private async loadPage() {
      this.pageTitle = this.page.title;
      this.type = this.page.type;
      this.pageForm.patchValue({
        'body': this.page.body,
        'image_id': this.page.image_id
      });
      if(this.page.url) {
        this.pageForm.patchValue({
          'url': this.page.url
        });
        this.bannerImage = await this.utilService.getBase64FromUrl(this.page.url);
      }

      if (this.page.type === "grid") {
        this.gridList = this.page.cards;
      }
    }

    private buildForm() {
      this.pageForm = this.fb.group({
        body: ["", [Validators.required, Validators.minLength(25)]],
        image_id: [null, Validators.required],
      });
    }

    public handleBannerUploaded(imageUploaded: boolean){
      this.hasImage = imageUploaded;
    }

    public async save() {
      this.loading = true;
      if (this.hasImage) {
        const { success, message, id, url } = await this.imageUploaderComponent.submit();
        if (success) {
          this.pageForm.patchValue({ image_id: id });
          this.page.url = url;
        }
      }
      this.pageForm.value.type = this.type;
      this.type === "grid" ? (this.pageForm.value.cards = this.gridList) : "";
      const data = this.pageForm.value;
      this.httpService.post("pages/edit/" + this.page.id, data).then(({ success }) => {
        if (success) {
          this.page = { ...this.page, ...data };
          this.utilService.notification( "The page was updated with no problems." );
        } else {
          this.utilService.notification( "There was a problem updating the webpage" );
        }
        this.loading = false;
      });
    }

    public getBanner() {
      return {
        backgroundImage: this.page.url
        ? `url(${this.page.url})`
        : 'url("/assets/img/noBanner.png")',
        backgroundPosition: "center",
        backgroundSize: "cover",
      };
    }
    @HostListener('window:resize', ['$event'])
    onResize(event): void {
      this.isMobile = event.target.innerWidth < 750;
      this.breakpoint = event.target.innerWidth <= 400 ? 1 : 4;
    }
  }
