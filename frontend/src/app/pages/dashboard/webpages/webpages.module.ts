import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebpagesComponent } from './webpages.component';
import { RouterModule } from '@angular/router';
import { HeaderModule } from 'projects/ng-utils/src/lib/header/header.module';
import { LoadingModule } from 'projects/ng-utils/src/lib/loading/loading.module';
import { SegmentModule } from 'projects/ng-utils/src/lib/segment/segment.module';
import { TableModule } from 'projects/ng-utils/src/lib/table/table.module';
import { ImageUploaderModule } from 'projects/ng-utils/src/lib/image-uploader/image-uploader.module';
import { WebpagesAmComponent } from './webpages-am/webpages-am.component';
import { WebpageResolver } from './webpage.resolver';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { NewElementDialogComponent } from './webpages-am/new-element-dialog/new-element-dialog.component';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  declarations: [ WebpagesComponent, WebpagesAmComponent, NewElementDialogComponent ],
  imports: [
    CommonModule,
    HeaderModule,
    SegmentModule,
    TableModule,
    LoadingModule,
    ImageUploaderModule,
    SharedModule,
    AngularEditorModule,
    RouterModule.forChild([
      { path: '', component: WebpagesComponent },
      {
        path: ':id', component: WebpagesAmComponent,
        resolve: {
          page: WebpageResolver
        } },

      ])
    ],
    exports: [
      RouterModule
    ],
    providers: [WebpageResolver]
  })
  export class WebpagesModule { }
