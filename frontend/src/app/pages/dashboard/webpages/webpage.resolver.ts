import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UtilService, HttpService } from '../../../core';

export interface Webpage{
  id?: number;
  name?: string;
  children?: Webpage[];
  slug?: string;
}

@Injectable()
export class WebpageResolver implements Resolve<Webpage> {
  constructor(
    private httpService: HttpService,
    private utilService: UtilService,
    private router: Router
    ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      return new Promise((resolve, reject) => {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
          // const loading = this.utilService.loading();
          return this.httpService.get('pages/' + id).then((article: Webpage) => {
            resolve(article);
            // loading.dismiss();
          }).catch(error => {
            console.error(error);
            // loading.dismiss();
            this.utilService.notification('Webpage not found with ID ' + id);
            this.router.navigateByUrl('/pages');
            reject();
          });
        } else {
          reject();
        }
      });
    }
  }
