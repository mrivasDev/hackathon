import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { RouterModule } from '@angular/router';
import { GradientCardModule } from 'projects/ng-utils/src/lib/gradientCard/gradientCard.module';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    GradientCardModule,
    RouterModule.forChild([{ path: '', component: HomeComponent }])
  ]
})
export class HomeModule { }
