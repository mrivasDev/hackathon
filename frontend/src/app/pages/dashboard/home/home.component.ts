import { Component, Inject, OnInit } from '@angular/core';
import { UtilService } from '../../../core';
import { User } from '../../../models';
import { GradientCardConfig } from 'projects/ng-utils/src/lib/gradientCard/gradientCard.component';
import { DashboardService } from '..';

@Component({
  selector: 'ad-infosec-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  constructor(
    private dashboardService: DashboardService,
    private utilService: UtilService
  ) { }

  user: User = null;

  cardConfig: GradientCardConfig = {
    closable: false,
    title: "Bienvenido/a",
    icon: "emoji_people",
    background: "#9CCD5B",
    imageUrl: "assets/img/logo.png"
  };

  ngOnInit() {
    this.dashboardService.breadcrumbs.emit("Inicio");
    this.user = this.utilService.getLS("user", true);
    this.cardConfig.title = `Bienvenido/a ${this.user.name} ${this.user.surname ? this.user.surname : ''}`;
    this.cardConfig.subtitle = `Email: ${this.user.email} - Roles: ${this.user.roles}`;
  }

}
