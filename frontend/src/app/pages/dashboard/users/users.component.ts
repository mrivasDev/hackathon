import { Component, OnInit, HostListener } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { FormLayout } from 'projects/ng-utils/src/lib/header/header.component';
import { ConfirmationMobileComponent } from 'projects/ng-utils/src/lib/confirmation/confirmationBottomSheet/confirmation.component';
import { ConfirmationDesktopComponent } from 'projects/ng-utils/src/lib/confirmation/confirmationModal/confirmation.component';
import { MainActionBottomSheetComponent } from 'projects/ng-utils/src/lib/mainActionBottomSheet/mainActionBottomSheet.component';
import { MainActionModalComponent } from 'projects/ng-utils/src/lib/mainActionModal/mainActionModal.component';
import { ActionTable, MobileSource } from 'projects/ng-utils/src/lib/table/table.component';
import { HttpService } from '../../../core/http.service';
import { User } from '../../../models/user.model';
import { UserColumn, columnsToDisplay, modalFormElements, TableSettings } from './userUtils';

@Component({
	selector: 'ad-infosec-users',
	templateUrl: './users.component.html',
	styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
	public isMobile: boolean = false;

	public dataSource: any[] = [];

	public mobileSource: any[] = [];

	public isLoading: boolean = false;

	public columnsToDisplay: UserColumn[] = columnsToDisplay;

	public modalTitle: string = 'Alta usuario';

	public modalSubTitle: string = 'Dar de alta a un usuario';

	public modalApiUrl: string = 'users';

	public modalForm: FormLayout[] = modalFormElements;

	public tableSettings: TableSettings = {
		pageSize: 0,
		pageIndex: 0,
		resultsLength: 0,
		pageSizeOptions: [15]
	}

	public tableActions: ActionTable[] = [
		{
			icon: 'edit',
			name: 'Editar',
			callback: (element) => { this.openEdit(element) },
		},
		{
			icon: 'delete',
			name: 'Eliminar',
			callback: (element) => { this.handleDelete(element) },
		},
	];


	constructor(public dialog: MatDialog, public bottomSheet: MatBottomSheet, private httpService: HttpService ) { }


	ngOnInit(): void {
		this.isMobile = document.documentElement.clientWidth <= 750;
		this.fetchUsers();
	}

	fetchUsers(searchText: string | number = ""): void {
		this.isLoading = true;
		this.httpService.get('users', { size: this.tableSettings.pageSize, page: this.tableSettings.pageIndex, searchText }).then(({ per_page, current_page, total, data }: any) => {
			this.tableSettings.pageSize = per_page;
			this.tableSettings.pageIndex = current_page - 1;
			this.tableSettings.resultsLength = total;
			this.dataSource = data;
			this.mobileSource = this.getMobileSource(data);
			this.isLoading = false;
		});
	}

	handlePageEvent(event: PageEvent) {
		if (event.pageSize !== this.tableSettings.pageSize) {
			this.tableSettings.pageSize = event.pageSize;
		}

		if (event.pageIndex !== this.tableSettings.pageIndex) {
			this.tableSettings.pageIndex = event.pageIndex;
		}

		this.fetchUsers();
	}

	handleFinishCreating(event: any) {
		this.fetchUsers();
	}

	openEdit(element) {
		if (this.isMobile) {
			this.openEditBottomSheet(element);
		} else {
			this.openEditDialog(element);
		}
	}

	openEditDialog(element) {
		const title = "Editar usuario";
		const subtitle = `${element.name} ${element.lastname}`;
		const editDialog = this.dialog.open(MainActionModalComponent, {
			width: '650px',
			height: 'auto',
			data: {
				title,
				subtitle,
				apiUrl: `${this.modalApiUrl}/edit/${element.id}`,
				form: this.modalForm,
				method: 'post',
				element
			}
		});
		editDialog.afterClosed().subscribe(result => {
			this.fetchUsers();
		});
	}

	openEditBottomSheet(element: MobileSource) {
		const user = this.dataSource.find((user: User) => user.id === element.id);
		const title = "Editar usuario";
		const subtitle = `${user.name ? user.name : ''} ${user.lastname ? user.lastname : ''}`;
		const editBottomSheet = this.bottomSheet.open(MainActionBottomSheetComponent, {
			data: {
				title,
				subtitle,
				apiUrl: `${this.modalApiUrl}/edit/${element.id}`,
				form: this.modalForm,
				method: 'post',
				element: user
			}
		});
		editBottomSheet.afterDismissed().subscribe(() => {
			this.fetchUsers();
		});
	}

	handleDelete(element) {
		if (this.isMobile) {
			this.openDeleteBottomSheet(element);
		} else {
			this.openDeleteDialog(element);
		}
	}

	openDeleteDialog(element: User) {
		const title = "Editar usuario";
		const subtitle = `${element.name}`;
		const editDialog = this.dialog.open(ConfirmationDesktopComponent, {
			width: '650px',
			height: 'auto',
			data: {
				title,
				subtitle,
				apiUrl: `${this.modalApiUrl}/delete/${element.id}`,
				form: this.modalForm,
				method: 'post',
				element
			}
		});
		editDialog.afterClosed().subscribe(result => {
			this.fetchUsers();
		});
	}

	openDeleteBottomSheet(element: MobileSource) {
		const user = this.dataSource.find((user: User) => user.id === element.id);
		const title = "Editar usuario";
		const subtitle = `${user.name ? user.name : ''} ${user.lastname ? user.lastname : ''}`;
		const deleteBottomSheet = this.bottomSheet.open(ConfirmationMobileComponent, {
			data: {
				title,
				subtitle,
				apiUrl: `${this.modalApiUrl}/delete/${element.id}`,
				form: this.modalForm,
				method: 'post',
				element: user
			}
		});
		deleteBottomSheet.afterDismissed().subscribe(() => {
			this.fetchUsers();
		});
	}

	getMobileSource(data: any[]): MobileSource[] {
		return data.map(({ id, name, dni, email, surname }: User): MobileSource => {
			const title = `${name}`;
			const description = `...`;
			const expandedInfo = [
				`Nombre: ${name ? name : 'Sin datos'}`,
				`Apellido: ${surname ? surname : 'Sin datos'}`,
				`Email: ${email ? email : 'Sin datos'}`,
				`DNI: ${dni ? dni : 'Sin datos'}`,
			];
			const mobileInfo = {
				id,
				title,
				description,
				expandedInfo
			};
			return mobileInfo;
		});
	}

	@HostListener('window:resize', ['$event'])
	onResize(event): void {
		this.isMobile = event.target.innerWidth < 750;
	}

}
