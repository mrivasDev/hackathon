import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users.component';
import { RouterModule } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { HeaderModule } from 'projects/ng-utils/src/lib/header/header.module';
import { LoadingModule } from 'projects/ng-utils/src/lib/loading/loading.module';
import { SegmentModule } from 'projects/ng-utils/src/lib/segment/segment.module';
import { TableModule } from 'projects/ng-utils/src/lib/table/table.module';

@NgModule({
  declarations: [UsersComponent],
  imports: [
    CommonModule,
    HeaderModule,
    SegmentModule,
    TableModule,
    MatListModule,
    MatIconModule,
    LoadingModule,
    RouterModule.forChild([{ path: '', component: UsersComponent }])
  ]
})
export class UsersModule { }
