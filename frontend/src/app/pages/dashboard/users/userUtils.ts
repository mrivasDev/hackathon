import { Validators } from "@angular/forms";
import { FormLayout } from "projects/ng-utils/src/lib/header/header.component";

export interface UserColumn {
	field: string, display: string
}

export interface TableSettings {
	pageSize: number,
	pageIndex: number,
	resultsLength: number,
	pageSizeOptions: number[]
}

export const modalFormElements: FormLayout[] = [
	{
		rowIndex: 1, element: [
			{ field: 'username', display: "Nombre de usuario", placeholder: "Nombre de usuario", validator: Validators.required, type: "text", element: 'input' }
		]
	},
	{
		rowIndex: 2, element: [
			{ field: 'password', display: "Contraseña", placeholder: "Contraseña", type: "password", element: 'input' }
		]
	},
	{
		rowIndex: 3, element: [
			{ field: 'name', display: "Nombre", placeholder: "Nombre", validator: Validators.required, type: "text", element: 'input' },
		]
	},
	{
		rowIndex: 4, element: [
			{ field: 'lastname', display: "Apellido", placeholder: "Apellido", validator: Validators.required, type: "text", element: 'input' }
		]
	},

	{
		rowIndex: 5, element: [
			{ field: 'email', display: "Email", placeholder: "Email", validator: [Validators.email, Validators.required], type: "email", element: 'input' },
		]
	},
	{
		rowIndex: 6, element: [
			{ field: 'phone', display: "Telefono", placeholder: "Telefono", type: "tel", element: 'input' }
		]
	},
	{
		rowIndex: 7, element: [
			{ field: 'cuil', display: "CUIL", placeholder: "CUIL", validator: [Validators.required, Validators.minLength(10), Validators.maxLength(11)], type: "number", min: 0, element: 'input' },
		]
	},
	{
		rowIndex: 8, element: [
			{ field: 'dni', display: "DNI", placeholder: "DNI", type: "number", min: 0, element: 'input' }

		]
	},
	{
		rowIndex: 9, element: [
			{

				field: 'role', display: "Rol", placeholder: "Rol", validator: Validators.required, element: "select", options: [
					{ value: 'admin', display: 'Admin' },
					{ value: 'client', display: 'Cliente' },
				]
			}
		]
	},
]

export const columnsToDisplay = [
	{ field: "name", display: "Nombre" },
	{ field: "lastname", display: "Apellido" },
	{ field: "email", display: "Email" },
	{ field: "display_role", display: "Rol" },
	{ field: "cuil", display: "CUIL" },
	{ field: "actions", display: "Acciones" },
];
