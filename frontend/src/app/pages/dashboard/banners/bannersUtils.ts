import { Validators } from "@angular/forms";
import { FormLayout } from "projects/ng-utils/src/lib/header/header.component";

export interface BannerColumn {
  field: string, display: string
}

export interface TableSettings {
  pageSize: number,
  pageIndex: number,
  resultsLength: number,
  pageSizeOptions: number[]
}

export const modalFormElements: FormLayout[] = [
  {
    rowIndex: 1, element: [
      { field: 'image_id', display: "Banner image", placeholder: "Banner image", type: "image", element: 'image' },
    ]
  },
  {
    rowIndex: 2, element: [
      { field: 'news_id', display: "Article or news", placeholder: "Article or news", type: "text", element: 'select', 'options': [] },
    ]
  },
  { rowIndex: 3, element: [
    { field: 'external_link', display: "External link", placeholder: "External link", type: "text", element: 'input' }
  ]},
]
