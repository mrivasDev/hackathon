import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BannersComponent } from './banners.component';
import { RouterModule } from '@angular/router';
import { HeaderModule } from 'projects/ng-utils/src/lib/header/header.module';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { LoadingModule } from 'projects/ng-utils/src/lib/loading/loading.module';
import { SegmentModule } from 'projects/ng-utils/src/lib/segment/segment.module';
import { TableModule } from 'projects/ng-utils/src/lib/table/table.module';

@NgModule({
  declarations: [BannersComponent],
  imports: [
    CommonModule,
    HeaderModule,
    SegmentModule,
    TableModule,
    MatListModule,
    MatIconModule,
    LoadingModule,
    RouterModule.forChild([{ path: '', component: BannersComponent }])
  ]
})
export class BannersModule { }
