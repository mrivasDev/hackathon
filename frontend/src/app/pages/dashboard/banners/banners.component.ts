import { Component, OnInit, HostListener } from '@angular/core';
import { DashboardService } from '..';
import { UtilService, HttpService } from '../../../core';
import { Banner } from '../../../models/banner.model';
import { News } from '../../../models/news.model';
import { Pagination } from '../../../models/pagination.model';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatDialog } from '@angular/material/dialog';
import { modalFormElements } from './bannersUtils';
import { FormLayout } from 'projects/ng-utils/src/lib/header/header.component';
import { MainActionModalComponent } from 'projects/ng-utils/src/lib/mainActionModal/mainActionModal.component';
import { MainActionBottomSheetComponent } from 'projects/ng-utils/src/lib/mainActionBottomSheet/mainActionBottomSheet.component';

@Component({
	selector: 'ad-infosec-banners',
	templateUrl: './banners.component.html',
	styleUrls: ['./banners.component.scss']
})
export class BannersComponent implements OnInit {
	public isMobile: boolean = false;
	public loading: boolean = false;

	public banners: Banner[] = [];

	public modalTitle: string = 'New';

	public modalSubTitle: string = 'Create new banner';

	public modalApiUrl: string = 'banners';

	public modalForm: FormLayout[] = modalFormElements;

	public noBannerIcon : string = 'sentiment_very_dissatisfied';
	public noBannerMessage : string = 'Looks like there is no banners, you can create them by clicking on "New"';

	constructor( private utilService: UtilService, private httpService: HttpService, private dashboardService: DashboardService, private bannersAMMobile: MatBottomSheet, private bannersAMDesktop: MatDialog ) { }

	ngOnInit(): void {
		this.dashboardService.breadcrumbs.emit('Banners');
		this.isMobile = document.documentElement.clientWidth <= 750;
		this.fetchBanners();
		this.fetchNews();
	}

	private fetchBanners() {
		this.loading = true;
		this.httpService.get('banners').then((banners: Banner[]) => {
			this.banners = banners;
			this.loading = false;
		});
	}

	private fetchNews() {
		this.httpService.get('news').then(({ data }: Pagination) => {
			modalFormElements[1].element[0].options = data.map(({id, title}: News) => ({ display: title, value: id }));
		});
	}

	public handleDelete(banner: Banner){
		this.httpService.post('banners/delete/' + banner.id).then((res) => {
			if (res.success) {
				this.utilService.notification('The banner was removed successfuly');
				this.ngOnInit();
			} else {
				this.utilService.notification('There was a problem by removing the banner');
			}
		});
	}

	public handleEdit(banner: Banner){
		if (this.isMobile) {
			this.openBottomSheet(banner);
		}else{
			this.openDialog(banner);
		}
	}

	private openBottomSheet(banner: Banner){
		const bannersAMRef = this.bannersAMMobile.open(MainActionBottomSheetComponent, {
			data: {
				title: 'Banner edit',
				subtitle: ' ',
				apiUrl: this.modalApiUrl,
				form: this.modalForm,
				element: banner,
				method: 'post'
			}
		});
		bannersAMRef.afterDismissed().subscribe((result: any) => {
			if (result) {
				this.fetchBanners();
			}
		});


	}
	private openDialog(banner: Banner){
		const bannersAMRef = this.bannersAMDesktop.open(MainActionModalComponent, {
			width: '650px',
			height: 'auto',
			data: {
				title: 'Banner edit',
				subtitle: ' ',
				apiUrl: this.modalApiUrl,
				form: this.modalForm,
				element: banner,
				method: 'post'
			}
		});

		bannersAMRef.afterClosed().subscribe((result: any) => {
			if (result) {
				this.fetchBanners();
			}
		});
	}

	handleFinishCreating(event: any) {
		this.fetchBanners();
	}

	@HostListener('window:resize', ['$event'])
	onResize(event): void {
		this.isMobile = event.target.innerWidth < 750;
	}
}
