import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/core/auth.service';
import { User } from 'src/app/models';


@Injectable({
	providedIn: 'root'
})
export class RoleGuard implements CanActivate {

	constructor(
		private authService: AuthService,
	) { }

	canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
		const roles = next.data['roles'] || [];
		return this.checkLogin(roles);
	}

	checkLogin(roles: string[]): Promise<boolean> {
		return new Promise(resolve => {
			this.authService.identity().then((user: User) => {
				if (user) {
					if (roles.length === 0) {
						resolve(true);
					} else {
						return this.authService.hasAnyRole(roles).then(response => {
							if (response) {
								resolve(true);
							} else {
								window.history.back();
								resolve(false);
							}
						});
					}
				} else {
					window.history.back();
					resolve(false);
				}
			});
		});
	}

}
