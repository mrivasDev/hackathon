import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TechnicalSecretaryComponent } from './technical-secretary.component';

describe('TechnicalSecretaryComponent', () => {
  let component: TechnicalSecretaryComponent;
  let fixture: ComponentFixture<TechnicalSecretaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TechnicalSecretaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnicalSecretaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
