import { Component, OnInit, HostListener } from '@angular/core';
import { DashboardService } from '../dashboard.service';

@Component({
	selector: 'ad-infosec-technical-secretary',
	templateUrl: './technical-secretary.component.html',
	styleUrls: ['./technical-secretary.component.scss']
})
export class TechnicalSecretaryComponent implements OnInit {
	public isMobile: boolean = false;

	constructor(private dashboardService: DashboardService) { }

	ngOnInit(): void {
		this.isMobile = document.documentElement.clientWidth <= 750;
		this.dashboardService.breadcrumbs.emit('Secretaría Técnica');
	}

	public redirect(page: string) {
		window.open('http://www.cpcelapampa.org.ar/index.php/page/'+page, "_blank");
	}

	@HostListener('window:resize', ['$event'])
	onResize(event): void {
		this.isMobile = event.target.innerWidth < 750;
	}

}

