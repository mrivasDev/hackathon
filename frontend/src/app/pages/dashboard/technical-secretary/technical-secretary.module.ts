import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TechnicalSecretaryComponent } from './technical-secretary.component';
import { RouterModule } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { HeaderModule } from 'projects/ng-utils/src/lib/header/header.module';
import { LoadingModule } from 'projects/ng-utils/src/lib/loading/loading.module';
import { SegmentModule } from 'projects/ng-utils/src/lib/segment/segment.module';
import { TableModule } from 'projects/ng-utils/src/lib/table/table.module';
import { MatCardModule } from '@angular/material/card';
import { ResolutionsModule } from './resolutions/resolutions.module';

@NgModule({
	declarations: [TechnicalSecretaryComponent],
	imports: [
		CommonModule,
		HeaderModule,
		SegmentModule,
		TableModule,
		MatListModule,
		MatIconModule,
		LoadingModule,
		MatCardModule,
		RouterModule.forChild([
			{ path: '', component: TechnicalSecretaryComponent },
			{ path: 'resoluciones', loadChildren: () => ResolutionsModule },
		])
	]
})
export class TechnicalSecretaryModule { }
