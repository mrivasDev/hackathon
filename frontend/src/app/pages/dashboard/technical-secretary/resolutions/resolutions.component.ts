import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { Resolution } from '../../../../models/resolution.model';
import { AutocompleteComponent } from 'projects/ng-utils/src/lib/autocomplete/autocomplete.component';
import { AuthService } from '../../../../core/auth.service';
import { HttpService } from '../../../../core/http.service';
import { MatDialog } from '@angular/material/dialog';
import { Pagination } from '../../../../models/pagination.model';
import { DashboardService } from '../..';
import { FlatTreeControl } from '@angular/cdk/tree';
import { FormGroup, FormControl } from '@angular/forms';
import { MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material/tree';
import { OrderList } from '../../../../models/orderList.model';
import * as dayjs from 'dayjs';
import { MatSelectChange } from '@angular/material/select';
import { FormLayout } from 'projects/ng-utils/src/lib/header/header.component';
import { modalFormElements } from './resolutionsUtils';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MainActionBottomSheetComponent } from 'projects/ng-utils/src/lib/mainActionBottomSheet/mainActionBottomSheet.component';
import { MainActionModalComponent } from 'projects/ng-utils/src/lib/mainActionModal/mainActionModal.component';
import { ConfirmationDesktopComponent } from 'projects/ng-utils/src/lib/confirmation/confirmationModal/confirmation.component';
import { ConfirmationMobileComponent } from 'projects/ng-utils/src/lib/confirmation/confirmationBottomSheet/confirmation.component';

interface YearNode {
	year?: number;
	children?: Resolution[];
}

let TREE_DATA: YearNode[] = [];

/** Flat node with expandable and level information */
interface FlatNode extends Resolution {
	expandable: boolean;
	level: number;
}


@Component({
	selector: 'ad-infosec-resolutions',
	templateUrl: './resolutions.component.html',
	styleUrls: ['./resolutions.component.scss']
})
export class ResolutionsComponent implements OnInit {

	@ViewChild('autocomplete') autocomplete: AutocompleteComponent;

	public isMobile: boolean = false;

	public selectedOrder: string = null;

	public invertedOrder: boolean = false;

	public loading: boolean = false;

	public loadingSeeMore: boolean = false;

	public resolutions: Resolution[] = [];

	public finished: boolean = false;

	public currentYear = dayjs().get('year');

	public canEdit: boolean = false;

	public selectedResolution: Resolution = null;

	public pageSize: number = 25;

	public pageIndex: number = 0;

	public displaySeeMore: boolean = true;

	public modalTitle: string = 'Nueva resolución';

	public modalSubTitle: string = 'Crear una nueva resolución';

	public modalApiUrl: string = 'resolutions';

	public modalForm: FormLayout[] = modalFormElements;

	public resolutionForm: FormGroup = new FormGroup({
		resolution: new FormControl()
	});

	public orderList: OrderList[] = [
		{ value: 'year', display: 'Ordenar por año' },
		{ value: 'subject', display: 'Ordenar alfabeticamente' },
		{ value: 'created_at', display: 'Ordenar por creación' }
	];

	public _transformer = (node: any, level: number) => ({
		id: node.id,
		description: node.description,
		subject: node.subject,
		year: node.year,
		upload_id: node.upload_id,
		upload: node.upload,
		expandable: !!node.children && node.children.length > 0,
		level: level
	});

	public treeControl = new FlatTreeControl<FlatNode>(node => node.level, node => node.expandable);
	public treeFlattener = new MatTreeFlattener(this._transformer, node => node.level, node => node.expandable, node => node.children);
	public dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
	public hasChild = (_: number, node: FlatNode) => node.expandable;

	constructor(private dashboardService: DashboardService, private authService: AuthService, private httpService: HttpService, private resolutionAMMobile: MatBottomSheet, private resolutionAMDesktop: MatDialog ) {}

	ngOnInit(): void {
		this.isMobile = document.documentElement.clientWidth <= 750;
		this.dashboardService.breadcrumbs.emit('Resoluciones Provinciales');
		this.selectedOrder = 'year';
		this.invertedOrder = true;
		this.loadMore();
		this.createYearTree();
		this.createYearTree();
		this.authService.can('editar secretaria tecnica').then(canEdit => {
			this.canEdit = canEdit;
		});
	}

	private	loadMore(fromSeeMore: boolean = false) {
		if (!fromSeeMore) {
			this.loading = true;
		} else {
			this.loadingSeeMore = true;
		}
		this.httpService.get('resolutions', {
			size: this.pageSize,
			page: this.pageIndex,
			orderField: this.selectedOrder ? this.selectedOrder : 'id',
			orderDirection: this.invertedOrder ? 'desc' : 'asc'
		}).then(({ data, from, to, total, last_page }: Pagination) => {
			if (!fromSeeMore) {
				this.loading = false;
			} else {
				this.loadingSeeMore = false;
			}
			this.resolutions.push(...data);
			this.pageIndex++;
			this.displaySeeMore = total > to;
		});
	}

	public createYearTree() {
		this.httpService.get('resolutions/tree').then((resolutionsTree: Resolution[]) => {
			if (resolutionsTree.length) {
				const output = resolutionsTree
				.slice(0)
				.reduce((accumulator, item) => {
					const index = accumulator.findIndex(({ year, children }) => (year === item.year));
					if (index === -1) {
						accumulator.push( { 'year': item.year, children: [item] })
					} else {
						accumulator[index].children.push(item);
					}
					return accumulator
				}, []);
				this.dataSource.data = output;
			}
		});
	}

	public handleEdit(resolucion: Resolution) {
		if (this.isMobile) {
			this.openBottomSheet(resolucion);
		}else{
			this.openDialog(resolucion);
		}
	}

	private openBottomSheet(resolucion: Resolution){
		const resolutionAMRef = this.resolutionAMMobile.open(MainActionBottomSheetComponent, {
			data: {
				title: 'Edicion de resolución',
				subtitle: ' ',
				apiUrl: this.modalApiUrl,
				form: this.modalForm,
				element: resolucion,
				method: 'post'
			}
		});
		resolutionAMRef.afterDismissed().subscribe((result: any) => {
			if (result) {
				this.pageIndex = 0;
				this.resolutions = [];
				this.loadMore();
				this.createYearTree();
			}
		});


	}
	private openDialog(resolucion: Resolution){
		const resolutionAMRef = this.resolutionAMDesktop.open(MainActionModalComponent, {
			width: '650px',
			height: 'auto',
			data: {
				title: 'Edicion de resolución',
				subtitle: ' ',
				apiUrl: this.modalApiUrl,
				form: this.modalForm,
				element: resolucion,
				method: 'post'
			}
		});

		resolutionAMRef.afterClosed().subscribe((result: any) => {
			if (result) {
				this.pageIndex = 0;
				this.resolutions = [];
				this.loadMore();
				this.createYearTree();
			}
		});
	}


	public handleDelete(resolucion: Resolution) {
		if (this.isMobile) {
			this.openDeleteBottomSheet(resolucion);
		} else {
			this.openDeleteDialog(resolucion);
		}
	}

	public openDeleteDialog(resolucion: Resolution) {
		const title = "Eliminar resolucion";
		const subtitle = `${resolucion.description}`;
		const editDialog = this.resolutionAMDesktop.open(ConfirmationDesktopComponent, {
			width: '650px',
			height: 'auto',
			data: {
				title,
				subtitle,
				apiUrl: `${this.modalApiUrl}/delete/${resolucion.id}`,
				form: this.modalForm,
				method: 'post',
				element: resolucion,
				saveLabel: 'Eliminar'
			}
		});
		editDialog.afterClosed().subscribe((result: any) => {

			if (result) {
				this.pageIndex = 0;
				this.resolutions = [];
				this.loadMore();
				this.createYearTree();
			}
		});
	}

	public openDeleteBottomSheet(resolucion: Resolution) {
		const title = "Eliminar noticia";
		const subtitle = `${resolucion.description}`;
		const deleteBottomSheet = this.resolutionAMMobile.open(ConfirmationMobileComponent, {
			data: {
				title,
				subtitle,
				apiUrl: `${this.modalApiUrl}/delete/${resolucion.id}`,
				form: this.modalForm,
				method: 'post',
				element: resolucion,
				saveLabel: 'Eliminar'
			}
		});
		deleteBottomSheet.afterDismissed().subscribe((result: any) => {
			console.log(result);
			if (result) {
				this.pageIndex = 0;
				this.resolutions = [];
				this.loadMore();
				this.createYearTree();
			}
		});
	}

	public onResolutionSelect(resolution: Resolution) {
		if (resolution) {
			this.selectedResolution = resolution;
		}
	}

	public handleSeachSelect($event: any){
		this.selectedResolution = $event;
	}

	public onDeselect(){
		this.selectedResolution = null;
		this.autocomplete.cleanSearch();
	}

	public download(resolution: Resolution) {
		this.authService.can('ver secretaria tecnica').then(canView => {
			if (canView) {
				this.httpService.get('resolutions/' + resolution.id).then((res: Resolution) => {
					this.httpService.get('upload/generate/' + res.upload.hash).then(access => {
						window.open(`${this.httpService.baseUrl}upload/${access.token}`);
					});
				});
			}
		});
	}

	public handleChangeOrder({ value }: MatSelectChange) {
		this.pageIndex = 0;
		this.pageIndex = 0;
		this.resolutions = [];
		this.loadMore();
		this.createYearTree();
	}

	public toggleExpandedDescription(index: number, type: number) {
		if (this.resolutions[index].description.length > 30) {
			this.resolutions[index].expandedUI = !this.resolutions[index].expandedUI;
		}
	}

	public trackByFn(index: number, item: any): string {
		return item ? `resolution-prov-${item.id}` : null;
	}

	@HostListener('window:resize', ['$event'])
	onResize(event): void {
		this.isMobile = event.target.innerWidth < 750;
	}
}
