import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResolutionsComponent } from './resolutions.component';
import { RouterModule } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { HeaderModule } from 'projects/ng-utils/src/lib/header/header.module';
import { LoadingModule } from 'projects/ng-utils/src/lib/loading/loading.module';
import { SegmentModule } from 'projects/ng-utils/src/lib/segment/segment.module';
import { TableModule } from 'projects/ng-utils/src/lib/table/table.module';
import { AutocompleteModule } from 'projects/ng-utils/src/lib/autocomplete/autocomplete.module';
import { MatButtonModule } from '@angular/material/button';
import { MatTreeModule } from '@angular/material/tree';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { MatMenuModule } from '@angular/material/menu';
import { FormsModule } from '@angular/forms';



@NgModule({
	declarations: [ResolutionsComponent],
	imports: [
		FormsModule,
		CommonModule,
		HeaderModule,
		SegmentModule,
		TableModule,
		MatListModule,
		MatIconModule,
		LoadingModule,
		MatCardModule,
		MatButtonModule,
		MatTreeModule,
		MatTabsModule,
		MatCheckboxModule,
		MatSelectModule,
		AutocompleteModule,
		MatMenuModule,
		RouterModule.forChild([
			{ path: '', component: ResolutionsComponent }
		])
	]
})
export class ResolutionsModule { }
