import { Validators } from "@angular/forms";
import { FormLayout } from "projects/ng-utils/src/lib/header/header.component";

export interface TableSettings {
	pageSize: number,
	pageIndex: number,
	resultsLength: number,
	pageSizeOptions: number[]
}

export const modalFormElements: FormLayout[] = [
	{
		rowIndex: 1, element: [{ field: 'description', display: "Descripción", placeholder: "Descripción", type: "text", element: 'input' }]
	},
	{
		rowIndex: 2, element: [{ field: 'subject', display: "Tema", placeholder: "Tema", type: "text", element: 'input' }]
	},
	{
		rowIndex: 3, element: [{ field: 'year', display: "Año", placeholder: "Año", type: "number", element: 'input' }]
	},
	{
		rowIndex: 4, element: [{ field: 'upload_id', display: "Subir resolucion", placeholder: "Subir resolucion", type: "file", element: 'file' }]
	},
]
