import { Injectable, EventEmitter } from '@angular/core';
import { UtilService } from '../../core';

@Injectable()
export class DashboardService {

  isSnavOpened: boolean = true;

  isConfNavOpened: boolean = false;

  isDarkTheme: boolean | null = null;

  themeColor: 'primary' | 'accent' = 'primary';

  constructor(private utilService: UtilService) {

    this.isDarkTheme = this.utilService.getLS('isDarkTheme', true);

    this.onSidenavToggle.subscribe(() => {
      this.isSnavOpened = !this.isSnavOpened;
    });

    this.onConfNavToggle.subscribe(() => {
      this.isConfNavOpened = !this.isConfNavOpened;
    })

    this.onDarkThemeToggle.subscribe(() => {
      this.isDarkTheme = !this.isDarkTheme;
      this.utilService.setLS('isDarkTheme', this.isDarkTheme, true);
    });

    this.onPrimaryThemeSelected.subscribe(() => {
      this.themeColor = 'primary';
      this.utilService.setLS('themeColor', this.themeColor, true);
    });

    this.onAccentThemeSelected.subscribe(() => {
      this.themeColor = 'accent';
      this.utilService.setLS('themeColor', this.themeColor, true);
    });
  }

  onSidenavToggle: EventEmitter<any> = new EventEmitter<any>();

  onConfNavToggle: EventEmitter<any> = new EventEmitter<any>();

  onDarkThemeToggle: EventEmitter<any> = new EventEmitter<any>();

  onPrimaryThemeSelected: EventEmitter<any> = new EventEmitter<'primary' | 'accent'>();

  onAccentThemeSelected: EventEmitter<any> = new EventEmitter<'primary' | 'accent'>();

  breadcrumbs = new EventEmitter();

}
