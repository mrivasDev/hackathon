import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/auth.service';

@Component({
  selector: 'ad-infosec-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

	constructor(
		private authService: AuthService
	) { }

	refreshInterval = null;

	ngOnInit() {
		this.refresh();
	}

	refresh() {
		if (!this.refreshInterval) {
			this.refreshInterval = setInterval(() => {
				this.refresh();
			}, 180000);
		}
	}

	logout() {
		this.authService.logout();
	}
}
